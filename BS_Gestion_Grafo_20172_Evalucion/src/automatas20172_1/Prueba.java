package automatas20172_1;

import java.util.Arrays;

/**
 *
 * @author BS
 */
public class Prueba {

    public static void main(String[] args) {
        ///Alt +28∟
        ///Se crea el Objeto Grafo
        Grafo grafo = new Grafo();
        ///Se agregan cada uno de los estados
        ///seguidos de true si es inicial, y luego true si es final
        grafo.crearEstadoYObtener("A", true, false);
        grafo.crearEstadoYObtener("B", false, false);
        grafo.crearEstadoYObtener("C", false, false);
        grafo.crearEstadoYObtener("D", false, true);
        grafo.crearEstadoYObtener("E", false, false);
        grafo.crearEstadoYObtener("F", false, true);
        grafo.crearEstadoYObtener("G", false, true);
        grafo.agregarEstadoTransicion("A", "B", "b");
//        grafo.agregarEstadoTransicion("A", "A", "b");
        grafo.agregarEstadoTransicion("A", "C", "c");
        grafo.agregarEstadoTransicion("B", "D", "d");
        grafo.agregarEstadoTransicion("B", "C", "c");
        grafo.agregarEstadoTransicion("C", "D", "d");
        grafo.agregarEstadoTransicion("A", "D", "");
        grafo.agregarEstadoTransicion("C", "E", "");
        grafo.agregarEstadoTransicion("E", "F", "m");
        grafo.agregarEstadoTransicion("E", "G", "n");
//        grafo.agregarEstadoTransicion("B", "C", "b");
//        grafo.agregarEstadoTransicion("C", "C", "a b");
//        grafo.agregarEstadoTransicion("C", "C", "b");
        ///En caso que haya dos transiciones de un estado a otro se puede colocar separado
        ///por espacio

        System.out.println(grafo);
        Arrays.asList("bcm", "bcn", "bcda", "bcd", "bd", "acb", "abb", "acba", "abc").forEach(
                s -> System.out.println("Resultado " + s + " " + grafo.evaluarCadena(s))
        );
    }

    public static boolean evaluarMatriz(String[][] matriz, String cadena) {
        Grafo grafo = new Grafo();
        for (int fila = 1; fila < matriz.length; fila++) {
            for (int columna = 1; columna < matriz[fila].length; columna++) {
                grafo.agregarEstadoTransicion(matriz[fila][0], matriz[0][columna], matriz[fila][columna]);
            }
        }
        boolean sw=grafo.evaluarCadena(cadena);
        System.out.println("Evaluar "+cadena+"->"+sw);
        return sw;
    }

}
