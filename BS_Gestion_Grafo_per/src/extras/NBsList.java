package extras;

public class NBsList {
    private NBsList sig;//lista doblemente enlazada
    private NBsList ant;
    private Object obj=null;//es un objeto guardado en la lista
    private Object objrelacion=null;//es un objeto que relaciona al primero;
    private double rec=0;//es un numero que identifica al(os)objeto(s)
    private boolean estado=false;//es otra identificacion de los objetos
    public NBsList(Object obj,Object objrelacion){
        this.obj=obj;
        this.objrelacion=objrelacion;
    }
    public NBsList(Object obj,double rec) {
        this.obj = obj;
        this.rec=rec;
    }
    public NBsList(Object obj,Object obj2, double rec, boolean estado) {
        this.obj = obj;
        this.objrelacion=obj2;
        this.rec=rec;
        this.estado=estado;
    }
    public NBsList getSig() {
        return sig;
    }    public void setSig(NBsList sig) {
        this.sig = sig;
    }    public NBsList getAnt() {
        return ant;
    }    public void setAnt(NBsList ant) {
        this.ant = ant;
    }    public Object getObj() {
        return obj;
    }    public void setObj(Object obj) {
        this.obj = obj;
    }    public double getRec() {
        return rec;
    }    public void setRec(double rec) {
        this.rec = rec;
    }    public Object getObjrelacion() {
        return objrelacion;
    }    public void setObjrelacion(Object objrelacion) {
        this.objrelacion = objrelacion;
    }    public boolean isEstado() {
        return estado;
    }    public void setEstado(boolean estado) {
        this.estado = estado;
    }
}
