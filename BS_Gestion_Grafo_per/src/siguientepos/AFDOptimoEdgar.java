/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package siguientepos;

import Gestion_grafo.Implementacion_Grafo;
import clases.Grafo;
import clases.Vertice;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

/**
 *
 * @author BS
 */
public class AFDOptimoEdgar {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Hoja concatenacion[] = new Hoja[2];
        concatenacion[0] = new Hoja("a", new Integer[]{1, 5, 7, 10});
        concatenacion[1] = new Hoja("b", new Integer[]{2, 3, 4, 6, 8, 9});
        //Numero de hojas totales
        int numeroHojas = 11;
        SiguientePos siguiente[] = new SiguientePos[numeroHojas + 1];
        siguiente[1] = new SiguientePos(new Integer[]{6, 7, 11});
        siguiente[2] = new SiguientePos(new Integer[]{3});
        siguiente[3] = new SiguientePos(new Integer[]{4, 5});
        siguiente[4] = new SiguientePos(new Integer[]{4, 5});
        siguiente[5] = new SiguientePos(new Integer[]{6, 7, 11});
        siguiente[6] = new SiguientePos(new Integer[]{6, 7, 11});
        siguiente[7] = new SiguientePos(new Integer[]{8});
        siguiente[8] = new SiguientePos(new Integer[]{9, 10});
        siguiente[9] = new SiguientePos(new Integer[]{9, 10});
        siguiente[10] = new SiguientePos(new Integer[]{6,7,11});
        siguiente[11] = new SiguientePos(new Integer[]{});

        List<Conjunto> conjuntos = new LinkedList();
        int numConjunto = 1;
        int numConjuntoAEvaluar = 1;
        Conjunto conjuntoPrincipal = new Conjunto("C" + numConjunto);
        conjuntoPrincipal.setHojas(new Integer[]{1, 2});

        Conjunto conjuntoEvaluando = conjuntoPrincipal;
        boolean sw = true;
        System.out.println("ppos(raiz)=" + conjuntoEvaluando);

        ////////
        Object matriz[][] = new Object[100][concatenacion.length + 1];
        ////////
        List<Integer> inter;
        do {
            System.out.println("------");
            System.out.println(conjuntoEvaluando);
            matriz[numConjuntoAEvaluar][0] = conjuntoEvaluando.nombre;
            for (int i = 0; i < concatenacion.length; i++) {
                System.out.append("Trans(" + conjuntoEvaluando.nombre + "," + concatenacion[i].caracter + ")={");
                Conjunto conjuntoTemporal = new Conjunto(null);
                inter = interseccion(conjuntoEvaluando.hojas, concatenacion[i].hojas);
                for (Integer interseccion : inter) {
                    conjuntoTemporal.agregarHoja(siguiente[interseccion].siguiente);
                }
                conjuntoTemporal.ordenar();
                System.out.append(vectorACadena(conjuntoTemporal.hojas.toArray(), ","));
                System.out.append("}");
                int indexBusqueda = conjuntos.indexOf(conjuntoTemporal);
                if (indexBusqueda == -1) {
                    if (!conjuntoTemporal.esVacio()) {
                        numConjunto++;
                        conjuntoTemporal.nombre = "C" + numConjunto;
                        conjuntos.add(conjuntoTemporal);
                    }
                } else {
                    conjuntoTemporal = conjuntos.get(indexBusqueda);
                }
                if (conjuntoTemporal.esVacio()) {
                    matriz[numConjuntoAEvaluar][i + 1] = "";
                }
                matriz[numConjuntoAEvaluar][i + 1] = conjuntoTemporal.nombre;
                if (conjuntoTemporal.nombre != null) {
                    System.out.append("=").append(conjuntoTemporal.nombre);
                }
                System.out.append("\n");
            }
            numConjuntoAEvaluar++;
            if (numConjuntoAEvaluar <= conjuntos.size() + 1) {
                conjuntoEvaluando = conjuntos.get(numConjuntoAEvaluar - 2);
            } else {
                sw = false;
            }
        } while (sw);

        System.out.println("Matriz de adyacencia");
        for (int i = 0; i < numConjuntoAEvaluar; i++) {
            if (i == 0) {
                for (int k = 0; k < concatenacion.length + 1; k++) {
                    if (k > 0) {
                        System.out.append("|");
                    }
                    if (k == 0) {

                    } else {
                        System.out.append(concatenacion[k - 1].caracter);
                    }
                }
                System.out.append("\n");
            } else {
                for (int j = 0; j < matriz[i].length; j++) {
                    if (j == 0 && i == 0) {
                        continue;
                    }
                    if (j > 0) {
                        System.out.append("|");
                    }
                    System.out.append(matriz[i][j] == null ? "null" : matriz[i][j].toString());
                }
                System.out.append("\n");
            }
        }
        System.out.println("");
        /*for (Conjunto con : conjuntos) {
         if (con.contiene(new Integer[]{35, 36, 37, 38, 39, 40, 41, 42})) {
         System.out.append(con.nombre + ",");
         }
         }*/
    }

    public static String vectorACadena(Object[] lista, String separador) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < lista.length; i++) {
            if (sb.length() != 0) {
                sb.append(separador);
            }
            sb.append(lista[i].toString());
        }
        return sb.toString();
    }

    public static List<Integer> interseccion(List<Integer> lista1, List<Integer> lista2) {
        Conjunto cojunto = new Conjunto("");
        for (Integer elemento1 : lista1) {///Se verifica cuales elementos de la lista1 se encuentran en la lista2
            if (lista2.indexOf(elemento1) != -1) {
                cojunto.agregarHoja(elemento1);
            }
        }
        return cojunto.hojas;

    }

    public static List<Integer> cerraduraLanda(Grafo grafo, List<Integer> estadosDeEvaluacion) {
        List<Integer> estados = new LinkedList<>();
        List<Vertice> transicionesLanda = new LinkedList<>();
        for (Integer es : estadosDeEvaluacion) {
            Vertice vertice = grafo.buscar_vertice(es + "");
            transicionesLanda = vertice.buscarVerticesConPeso(0);
            transicionesLanda.add(new Vertice(es + ""));
            for (int i = 0; i < transicionesLanda.size(); i++) {
                Vertice translanda = transicionesLanda.get(i);
                transicionesLanda.addAll(translanda.buscarVerticesConPeso(0));
            }
        }
        for (Vertice vertice : transicionesLanda) {
            if (estados.indexOf(Integer.parseInt(vertice.getNomVer())) == -1) {
                estados.add(Integer.parseInt(vertice.getNomVer()));
            }
        }
        Collections.sort(estados);
        return estados;
    }

    public static List<Integer> cerraduraLanda(Vertice estado) {
        List<Integer> estados = new LinkedList<>();
        int idVertice = Integer.parseInt(estado.getNomVer());
        estados.add(idVertice);
        List<Vertice> cerraduraLanda = estado.buscarVerticesConPeso(0);
        for (Vertice v : cerraduraLanda) {
            idVertice = Integer.parseInt(v.getNomVer());
            if (estados.indexOf(idVertice) == -1) {
                estados.add(idVertice);
            }
        }
        return estados;
    }

    public static void iniciarAnalisisGrafoPorThomson() {
        Hoja concatenacion[] = new Hoja[3];
        concatenacion[0] = new Hoja("", new Integer[]{});
        concatenacion[1] = new Hoja("a", new Integer[]{});
        concatenacion[2] = new Hoja("b", new Integer[]{});

        Grafo grafo = Implementacion_Grafo.grafo;
        List<Conjunto> conjuntos = new LinkedList();

        int numConjunto = 0;
        int numConjuntoAEvaluar = 0;
        Conjunto conjuntoPrincipal = new Conjunto("0");
        conjuntoPrincipal.setHojas(new Integer[]{0});
        Conjunto conjuntoEvaluando = conjuntoPrincipal;
        ////////
        Object matriz[][] = new Object[100][concatenacion.length + 1];
        ////////        
        System.out.println("------");
        System.out.println("Cerradura_L(" + conjuntoEvaluando.mostrarConjunto() + ")");
        List<Integer> transicionesLanda = cerraduraLanda(grafo, conjuntoEvaluando.hojas);
        Conjunto conjuntoTemporal = new Conjunto(null);
        conjuntoTemporal.agregarHoja(transicionesLanda);
//            matriz[numConjuntoAEvaluar][0] = conjuntoEvaluando.nombre;
        /////////////////////
        int indexBusqueda = conjuntos.indexOf(conjuntoTemporal);
        if (indexBusqueda == -1) {
            if (!conjuntoTemporal.esVacio()) {
                numConjunto++;
                conjuntoTemporal.nombre = "C" + (numConjunto);
                conjuntos.add(conjuntoTemporal);
            }
        } else {
            conjuntoTemporal = conjuntos.get(indexBusqueda);
        }
        /////////////////
        System.out.append("={" + conjuntoTemporal.mostrarConjunto() + "}=" + conjuntoTemporal.nombre);
        System.out.println("");
        boolean sw = true;
        do {
            if (!conjuntoTemporal.isEvaluado()) {
                for (int i = 1; i < concatenacion.length; i++) {
                    System.out.append("  Mueve(" + conjuntoTemporal.nombre + "," + concatenacion[i].caracter + ")={");
                    Conjunto conjuntoTemporal2 = new Conjunto(null);
                    for (Integer hojasConjunto : conjuntoTemporal.hojas) {
                        Vertice vertice = grafo.buscar_vertice(hojasConjunto + "");
                        List<Vertice> transicionesConPeso = vertice.buscarVerticesConPeso(i);
                        conjuntoTemporal2.agregarHoja(verticesAListaEnteros(transicionesConPeso));
                    }
                    conjuntoTemporal2.ordenar();
                    System.out.append(vectorACadena(conjuntoTemporal2.hojas.toArray(), ","));
                    System.out.append("}");
                    System.out.println("");
                    System.out.println("    ---Cerradura_L(" + conjuntoTemporal2.mostrarConjunto() + ")");
                    List<Integer> transiciones = cerraduraLanda(grafo, conjuntoTemporal2.hojas);
                    Conjunto conjuntoTemporal3 = new Conjunto("");
                    conjuntoTemporal3.setHojas(transiciones);
                    indexBusqueda = conjuntos.indexOf(conjuntoTemporal3);
                    if (indexBusqueda == -1) {
                        if (!conjuntoTemporal3.esVacio()) {
                            numConjunto++;
                            conjuntoTemporal3.nombre = "C" + (numConjunto);
                            conjuntos.add(conjuntoTemporal3);
                        }
                    } else {
                        conjuntoTemporal3 = conjuntos.get(indexBusqueda);
                    }
                    System.out.append("     ={" + conjuntoTemporal3.mostrarConjunto() + "}=" + conjuntoTemporal3.nombre);
                    System.out.println("");

                    matriz[numConjuntoAEvaluar][i + 1] = conjuntoTemporal3.nombre;
                    //System.out.println("Matriz[" + numConjuntoAEvaluar + "][" + (i + 1) + "]=" + conjuntoTemporal3.nombre);
                    System.out.append("\n");
                }
                conjuntoTemporal.setEvaluado(true);
            }
            numConjuntoAEvaluar++;
            if (numConjuntoAEvaluar <= conjuntos.size()) {
                conjuntoEvaluando = conjuntos.get(numConjuntoAEvaluar - 1);
            } else {
                sw = false;
            }
            conjuntoTemporal=conjuntoEvaluando;
        } while (sw);
        /////Conjuntos Evaluados
        for (Conjunto conjunto : conjuntos) {
            //   System.out.println(conjunto.nombre);
        }
        /////
        if (true)return;
        System.out.println("Matriz de adyacencia");
        for (int i = 0; i < numConjuntoAEvaluar; i++) {
            if (i == 0) {
                for (int k = 0; k < concatenacion.length + 1; k++) {
                    if (k > 0) {
                        System.out.append("|");
                    }
                    if (k == 0) {

                    } else {
                        System.out.append(concatenacion[k - 1].caracter);
                    }
                }
                System.out.append("\n");
            } else {
                for (int j = 0; j < matriz[i].length; j++) {
                    if (j == 0 && i == 0) {
                        continue;
                    }
                    if (j > 0) {
                        System.out.append("|");
                    }
                    System.out.append(matriz[i][j] == null ? "null" : matriz[i][j].toString());
                }
                System.out.append("\n");
            }
        }
        System.out.println("");

    }

    public static int indiceDelConjuntoConNombre(List<Conjunto> conjunto, String nombre) {
        int index = -1;
        for (Conjunto c : conjunto) {
            index++;
            if (c.nombre.equalsIgnoreCase(nombre)) {
                break;
            }
        }
        return index;
    }

    public static List<Integer> verticesAListaEnteros(List<Vertice> vertices) {
        List<Integer> lista = new LinkedList<>();
        for (Vertice vertice : vertices) {
            try {
                lista.add(Integer.parseInt(vertice.getNomVer()));
            } catch (Exception e) {
            }
        }
        return lista;
    }
}
