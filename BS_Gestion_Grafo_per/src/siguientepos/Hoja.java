/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package siguientepos;

import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

/**
 *
 * @author BS
 */
public class Hoja {
    String caracter="";
    List<Integer> hojas=new LinkedList<>();
    public Hoja(String caracter, Integer[] numeroDeHoja){
        this.caracter=caracter;
        hojas.addAll(Arrays.asList(numeroDeHoja));
        Collections.sort(hojas);
    }
}
