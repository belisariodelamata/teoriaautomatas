/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * Implementacion_Grafo.java
 *
 * Created on 16/03/2009, 07:27:09 PM
 */

package Gestion_grafo;

import clases.Grafo;
import com.sun.java.swing.plaf.windows.WindowsLookAndFeel;
import java.awt.Graphics;
import java.awt.Point;
import java.io.IOException;
import javax.swing.JOptionPane;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import siguientepos.AFDOptimoEdgar;
/**
 *
 * @author jhernandez
 */
public class Implementacion_Grafo extends javax.swing.JFrame {
    public static Grafo grafo;
//    grafo_visual v;
    public f2 frecorridos;
    public f4 finfo;

    int warea=0,harea=0;

    /** Creates new form Implementacion_Grafo */
    public Implementacion_Grafo() {
        initComponents();
//        sabiasque.setVisible(true);
        warea=areagrafica.getWidth();
        harea=areagrafica.getHeight();
    }

    private void ponerTexto(String inicio, String fin){
        txtNombre.setText(inicio);
        txtVer_Fin.setText(fin);
        txtPeso.setText((int)(Math.random()*100)+"");
        btnAgregar_AdyActionPerformed(null);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        sabiasque = new javax.swing.JDialog();
        jLabel7 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        grupoExpansion = new javax.swing.ButtonGroup();
        txtNombre = new javax.swing.JTextField();
        txtVer_Fin = new javax.swing.JTextField();
        txtPeso = new javax.swing.JTextField();
        btnAgregar_Vertice = new javax.swing.JButton();
        btnModificar_vertice = new javax.swing.JButton();
        btnEliminar_vertice = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        btnAgregar_Ady = new javax.swing.JButton();
        btnModificar_adyacente = new javax.swing.JButton();
        btnEliminar_adyacente = new javax.swing.JButton();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jSeparator1 = new javax.swing.JSeparator();
        jLabel5 = new javax.swing.JLabel();
        jSeparator2 = new javax.swing.JSeparator();
        jLabel6 = new javax.swing.JLabel();
        txtNuevoNombre = new javax.swing.JTextField();
        jLabel8 = new javax.swing.JLabel();
        areagrafica = new javax.swing.JPanel();
        jScrollPane2 = new javax.swing.JScrollPane();
        txtinfoV = new javax.swing.JTextArea();
        checkdirigido = new javax.swing.JCheckBox();
        jSeparator3 = new javax.swing.JSeparator();
        btnorigen = new javax.swing.JButton();
        chkpesorReal = new javax.swing.JCheckBox();
        chkmostrarExpansion = new javax.swing.JCheckBox();
        panelExpansion = new javax.swing.JPanel();
        opcionKruskal = new javax.swing.JRadioButton();
        opcionPrim = new javax.swing.JRadioButton();
        chksoloexpansion = new javax.swing.JCheckBox();
        btnIniciarAnalisisThomson = new javax.swing.JButton();
        jMenuBar1 = new javax.swing.JMenuBar();
        jMenu1 = new javax.swing.JMenu();
        jMenuItem1 = new javax.swing.JMenuItem();
        jMenuItem2 = new javax.swing.JMenuItem();
        jMenu2 = new javax.swing.JMenu();
        menuInformacion = new javax.swing.JMenuItem();
        menueliminarVertices = new javax.swing.JMenuItem();
        menuEliminarAdys = new javax.swing.JMenuItem();
        menuadyacenciauno = new javax.swing.JMenuItem();
        menuVrecorridos = new javax.swing.JMenuItem();
        mnuorganizar = new javax.swing.JMenuItem();
        jSeparator4 = new javax.swing.JPopupMenu.Separator();
        menucuadricula = new javax.swing.JCheckBoxMenuItem();
        menuLimitar = new javax.swing.JCheckBoxMenuItem();

        sabiasque.setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        sabiasque.setResizable(false);
        sabiasque.getContentPane().setLayout(null);

        jLabel7.setFont(new java.awt.Font("Traditional Arabic", 0, 36)); // NOI18N
        jLabel7.setText("Sabias que...??");
        sabiasque.getContentPane().add(jLabel7);
        jLabel7.setBounds(70, 10, 240, 60);

        jLabel9.setText("jLabel9");
        sabiasque.getContentPane().add(jLabel9);
        jLabel9.setBounds(140, 120, 34, 14);

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("BS GRAPH BUILDER sep 2010");
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowActivated(java.awt.event.WindowEvent evt) {
                formWindowActivated(evt);
            }
            public void windowClosing(java.awt.event.WindowEvent evt) {
                formWindowClosing(evt);
            }
            public void windowClosed(java.awt.event.WindowEvent evt) {
                formWindowClosed(evt);
            }
            public void windowOpened(java.awt.event.WindowEvent evt) {
                formWindowOpened(evt);
            }
        });
        addComponentListener(new java.awt.event.ComponentAdapter() {
            public void componentResized(java.awt.event.ComponentEvent evt) {
                formComponentResized(evt);
            }
        });
        getContentPane().setLayout(null);

        txtNombre.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtNombreActionPerformed(evt);
            }
        });
        txtNombre.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtNombreKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtNombreKeyReleased(evt);
            }
        });
        getContentPane().add(txtNombre);
        txtNombre.setBounds(100, 10, 60, 20);

        txtVer_Fin.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtVer_FinActionPerformed(evt);
            }
        });
        txtVer_Fin.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtVer_FinKeyPressed(evt);
            }
        });
        getContentPane().add(txtVer_Fin);
        txtVer_Fin.setBounds(100, 40, 100, 20);

        txtPeso.setText("1");
        txtPeso.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtPesoActionPerformed(evt);
            }
        });
        getContentPane().add(txtPeso);
        txtPeso.setBounds(100, 90, 50, 20);

        btnAgregar_Vertice.setIcon(new javax.swing.ImageIcon(getClass().getResource("/extras/24-em-plus.png"))); // NOI18N
        btnAgregar_Vertice.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAgregar_VerticeActionPerformed(evt);
            }
        });
        getContentPane().add(btnAgregar_Vertice);
        btnAgregar_Vertice.setBounds(20, 150, 50, 35);

        btnModificar_vertice.setIcon(new javax.swing.ImageIcon(getClass().getResource("/extras/text_signature.png"))); // NOI18N
        btnModificar_vertice.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnModificar_verticeActionPerformed(evt);
            }
        });
        getContentPane().add(btnModificar_vertice);
        btnModificar_vertice.setBounds(140, 150, 50, 35);

        btnEliminar_vertice.setIcon(new javax.swing.ImageIcon(getClass().getResource("/extras/24-em-cross.png"))); // NOI18N
        btnEliminar_vertice.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEliminar_verticeActionPerformed(evt);
            }
        });
        getContentPane().add(btnEliminar_vertice);
        btnEliminar_vertice.setBounds(80, 150, 50, 35);

        jLabel1.setFont(new java.awt.Font("Courier New", 0, 12)); // NOI18N
        jLabel1.setText("Nuevo Nombre");
        getContentPane().add(jLabel1);
        jLabel1.setBounds(270, 20, 90, 14);

        btnAgregar_Ady.setIcon(new javax.swing.ImageIcon(getClass().getResource("/extras/24-em-plus.png"))); // NOI18N
        btnAgregar_Ady.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAgregar_AdyActionPerformed(evt);
            }
        });
        getContentPane().add(btnAgregar_Ady);
        btnAgregar_Ady.setBounds(20, 250, 50, 35);

        btnModificar_adyacente.setIcon(new javax.swing.ImageIcon(getClass().getResource("/extras/text_signature.png"))); // NOI18N
        btnModificar_adyacente.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnModificar_adyacenteActionPerformed(evt);
            }
        });
        getContentPane().add(btnModificar_adyacente);
        btnModificar_adyacente.setBounds(140, 250, 50, 35);

        btnEliminar_adyacente.setIcon(new javax.swing.ImageIcon(getClass().getResource("/extras/cut_red.png"))); // NOI18N
        btnEliminar_adyacente.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEliminar_adyacenteActionPerformed(evt);
            }
        });
        getContentPane().add(btnEliminar_adyacente);
        btnEliminar_adyacente.setBounds(80, 250, 50, 35);

        jLabel2.setFont(new java.awt.Font("Courier New", 0, 12)); // NOI18N
        jLabel2.setText("Vertice F");
        getContentPane().add(jLabel2);
        jLabel2.setBounds(20, 40, 140, 14);

        jLabel3.setFont(new java.awt.Font("Courier New", 0, 12)); // NOI18N
        jLabel3.setText("Arista");
        getContentPane().add(jLabel3);
        jLabel3.setBounds(20, 90, 80, 20);

        jLabel4.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Gestion_grafo/bs-quemado-mini.gif"))); // NOI18N
        getContentPane().add(jLabel4);
        jLabel4.setBounds(860, 0, 100, 80);
        getContentPane().add(jSeparator1);
        jSeparator1.setBounds(20, 120, 180, 20);

        jLabel5.setFont(new java.awt.Font("Courier New", 0, 12)); // NOI18N
        jLabel5.setText("ADYACENTES");
        getContentPane().add(jLabel5);
        jLabel5.setBounds(30, 210, 80, 14);
        getContentPane().add(jSeparator2);
        jSeparator2.setBounds(10, 200, 190, 10);

        jLabel6.setFont(new java.awt.Font("Courier New", 0, 12)); // NOI18N
        jLabel6.setText("VERTICES");
        getContentPane().add(jLabel6);
        jLabel6.setBounds(20, 130, 60, 14);

        txtNuevoNombre.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtNuevoNombreKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtNuevoNombreKeyReleased(evt);
            }
        });
        getContentPane().add(txtNuevoNombre);
        txtNuevoNombre.setBounds(270, 40, 70, 20);

        jLabel8.setFont(new java.awt.Font("Courier New", 0, 12)); // NOI18N
        jLabel8.setText("Vertice I");
        getContentPane().add(jLabel8);
        jLabel8.setBounds(20, 10, 140, 14);

        areagrafica.setForeground(new java.awt.Color(0, 0, 255));
        areagrafica.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        areagrafica.addMouseWheelListener(new java.awt.event.MouseWheelListener() {
            public void mouseWheelMoved(java.awt.event.MouseWheelEvent evt) {
                areagraficaMouseWheelMoved(evt);
            }
        });
        areagrafica.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                areagraficaMousePressed(evt);
            }
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                areagraficaMouseReleased(evt);
            }
        });
        areagrafica.addMouseMotionListener(new java.awt.event.MouseMotionAdapter() {
            public void mouseDragged(java.awt.event.MouseEvent evt) {
                areagraficaMouseDragged(evt);
            }
            public void mouseMoved(java.awt.event.MouseEvent evt) {
                areagraficaMouseMoved(evt);
            }
        });
        areagrafica.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                areagraficaKeyPressed(evt);
            }
        });
        getContentPane().add(areagrafica);
        areagrafica.setBounds(250, 110, 730, 470);

        txtinfoV.setBackground(new java.awt.Color(240, 240, 240));
        txtinfoV.setColumns(20);
        txtinfoV.setEditable(false);
        txtinfoV.setFont(new java.awt.Font("Monospaced", 0, 11)); // NOI18N
        txtinfoV.setRows(5);
        jScrollPane2.setViewportView(txtinfoV);

        getContentPane().add(jScrollPane2);
        jScrollPane2.setBounds(10, 320, 190, 200);

        checkdirigido.setFont(new java.awt.Font("Courier New", 0, 12)); // NOI18N
        checkdirigido.setText("Dirigido");
        checkdirigido.setOpaque(false);
        checkdirigido.setSelectedIcon(new javax.swing.ImageIcon(getClass().getResource("/extras/24-arrow-last.png"))); // NOI18N
        checkdirigido.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                checkdirigidoActionPerformed(evt);
            }
        });
        getContentPane().add(checkdirigido);
        checkdirigido.setBounds(20, 230, 90, 23);

        jSeparator3.setOrientation(javax.swing.SwingConstants.VERTICAL);
        getContentPane().add(jSeparator3);
        jSeparator3.setBounds(210, 100, 10, 470);

        btnorigen.setText("(0,0)");
        btnorigen.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnorigenActionPerformed(evt);
            }
        });
        btnorigen.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                btnorigenKeyPressed(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                btnorigenKeyTyped(evt);
            }
        });
        getContentPane().add(btnorigen);
        btnorigen.setBounds(800, 70, 60, 23);

        chkpesorReal.setFont(new java.awt.Font("Courier New", 0, 12)); // NOI18N
        chkpesorReal.setText("Peso real");
        chkpesorReal.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                chkpesorRealActionPerformed(evt);
            }
        });
        getContentPane().add(chkpesorReal);
        chkpesorReal.setBounds(20, 60, 100, 23);

        chkmostrarExpansion.setText("Mostrar Expansión Mínima");
        chkmostrarExpansion.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                chkmostrarExpansionActionPerformed(evt);
            }
        });
        getContentPane().add(chkmostrarExpansion);
        chkmostrarExpansion.setBounds(370, 30, 160, 23);

        panelExpansion.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        panelExpansion.setLayout(null);

        grupoExpansion.add(opcionKruskal);
        opcionKruskal.setFont(new java.awt.Font("Tahoma", 3, 12)); // NOI18N
        opcionKruskal.setForeground(new java.awt.Color(255, 0, 0));
        opcionKruskal.setSelected(true);
        opcionKruskal.setText("Kruskal");
        opcionKruskal.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                opcionKruskalActionPerformed(evt);
            }
        });
        panelExpansion.add(opcionKruskal);
        opcionKruskal.setBounds(20, 10, 69, 23);

        grupoExpansion.add(opcionPrim);
        opcionPrim.setFont(new java.awt.Font("Tahoma", 3, 12)); // NOI18N
        opcionPrim.setForeground(new java.awt.Color(255, 102, 0));
        opcionPrim.setText("Prim");
        opcionPrim.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                opcionPrimActionPerformed(evt);
            }
        });
        panelExpansion.add(opcionPrim);
        opcionPrim.setBounds(110, 10, 53, 23);

        chksoloexpansion.setFont(new java.awt.Font("Times New Roman", 3, 12)); // NOI18N
        chksoloexpansion.setText("Sólo expansión");
        chksoloexpansion.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                chksoloexpansionActionPerformed(evt);
            }
        });
        panelExpansion.add(chksoloexpansion);
        chksoloexpansion.setBounds(50, 30, 110, 23);

        getContentPane().add(panelExpansion);
        panelExpansion.setBounds(360, 40, 180, 60);

        btnIniciarAnalisisThomson.setText("Analisis AFD No Optimo");
        btnIniciarAnalisisThomson.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnIniciarAnalisisThomsonActionPerformed(evt);
            }
        });
        getContentPane().add(btnIniciarAnalisisThomson);
        btnIniciarAnalisisThomson.setBounds(600, 20, 180, 23);

        jMenu1.setText("Archivo");

        jMenuItem1.setText("Salir");
        jMenuItem1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem1ActionPerformed(evt);
            }
        });
        jMenu1.add(jMenuItem1);

        jMenuItem2.setText("Sabias que..");
        jMenuItem2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem2ActionPerformed(evt);
            }
        });
        jMenu1.add(jMenuItem2);

        jMenuBar1.add(jMenu1);

        jMenu2.setText("Vertices");
        jMenu2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenu2ActionPerformed(evt);
            }
        });

        menuInformacion.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_F1, 0));
        menuInformacion.setText("Información");
        menuInformacion.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuInformacionActionPerformed(evt);
            }
        });
        jMenu2.add(menuInformacion);

        menueliminarVertices.setIcon(new javax.swing.ImageIcon(getClass().getResource("/extras/application_osx_terminal.png"))); // NOI18N
        menueliminarVertices.setText("Eliminar Todos");
        menueliminarVertices.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menueliminarVerticesActionPerformed(evt);
            }
        });
        jMenu2.add(menueliminarVertices);

        menuEliminarAdys.setIcon(new javax.swing.ImageIcon(getClass().getResource("/extras/chart_line_delete.png"))); // NOI18N
        menuEliminarAdys.setText("Eliminar Adyacencias");
        menuEliminarAdys.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuEliminarAdysActionPerformed(evt);
            }
        });
        jMenu2.add(menuEliminarAdys);

        menuadyacenciauno.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Gestion_grafo/arrow_out.png"))); // NOI18N
        menuadyacenciauno.setText("Adyacencia uno a uno");
        menuadyacenciauno.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuadyacenciaunoActionPerformed(evt);
            }
        });
        jMenu2.add(menuadyacenciauno);

        menuVrecorridos.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_F4, 0));
        menuVrecorridos.setIcon(new javax.swing.ImageIcon(getClass().getResource("/extras/application_go.png"))); // NOI18N
        menuVrecorridos.setText("Recorridos");
        menuVrecorridos.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuVrecorridosActionPerformed(evt);
            }
        });
        jMenu2.add(menuVrecorridos);

        mnuorganizar.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_F5, 0));
        mnuorganizar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/extras/chart_organisation.png"))); // NOI18N
        mnuorganizar.setText("Organizar");
        mnuorganizar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mnuorganizarActionPerformed(evt);
            }
        });
        jMenu2.add(mnuorganizar);
        jMenu2.add(jSeparator4);

        menucuadricula.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_F6, 0));
        menucuadricula.setText("Rejilla");
        menucuadricula.setIcon(new javax.swing.ImageIcon(getClass().getResource("/extras/application_view_columns.png"))); // NOI18N
        menucuadricula.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menucuadriculaActionPerformed(evt);
            }
        });
        jMenu2.add(menucuadricula);

        menuLimitar.setSelected(true);
        menuLimitar.setText("Limitar");
        menuLimitar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/extras/arrow_in.png"))); // NOI18N
        jMenu2.add(menuLimitar);

        jMenuBar1.add(jMenu2);

        setJMenuBar(jMenuBar1);

        setSize(new java.awt.Dimension(1015, 656));
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void btnAgregar_VerticeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAgregar_VerticeActionPerformed
        grafo.agregar_vertices(txtNombre.getText());
    }//GEN-LAST:event_btnAgregar_VerticeActionPerformed
    @Override
    public void paint(Graphics g) {
        try{
            paintComponents(g);
/*            if (grafo != null) {
                for (int i = 0; i < this.getHeight(); i++) {
                    Graphics g2=this.getGraphics();
                    g2.setColor(Color.red);
                    g2.drawLine(0, i, this.getWidth(), i);
 
                }
            }
*/
                grafo.repaint();

        }catch(Exception e){

        }
    }

    private void btnAgregar_AdyActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAgregar_AdyActionPerformed
///los destinos puedo separarlos por ,
        try{
            String destinos[]=txtVer_Fin.getText().trim().split(",");
            grafo.setDibujarinmediato(false);
            for (int i = 0; i < destinos.length; i++) {
                grafo.agregar_ady(txtNombre.getText(), destinos[i], Integer.parseInt(txtPeso.getText()),checkdirigido.isSelected());
            }
            grafo.setDibujarinmediato(true);
            grafo.updateGraphics();
        }catch(Exception e){

        }

    }//GEN-LAST:event_btnAgregar_AdyActionPerformed

    private void btnModificar_verticeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnModificar_verticeActionPerformed
        try{
            boolean v=grafo.actualizar_vertice(txtNombre.getText(),txtNuevoNombre.getText());
            if (v){
                txtNombre.setText(txtNuevoNombre.getText());
                txtNuevoNombre.requestFocusInWindow();
                txtNuevoNombre.selectAll();
            }

        }catch(Exception e){

        }

    }//GEN-LAST:event_btnModificar_verticeActionPerformed

    private void btnEliminar_verticeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEliminar_verticeActionPerformed
        try{
        boolean v=grafo.eliminar_vertice(txtNombre.getText());
            if (v){
                txtNombre.requestFocus();
                txtNombre.selectAll();
            }
        }catch(Exception e){
        }
        grafo.updateGraphics();
    }//GEN-LAST:event_btnEliminar_verticeActionPerformed

    private void btnModificar_adyacenteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnModificar_adyacenteActionPerformed
        try{
        boolean v=grafo.actualizar_adyacente(txtNombre.getText(), txtVer_Fin.getText(), txtNuevoNombre.getText());
        grafo.updateGraphics();
        }catch(Exception e){

        }
    }//GEN-LAST:event_btnModificar_adyacenteActionPerformed

    private void btnEliminar_adyacenteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEliminar_adyacenteActionPerformed
        try{
        boolean v=grafo.eliminar_adyacente(txtNombre.getText(), txtVer_Fin.getText());

        grafo.updateGraphics();
        }catch(Exception e){

        }
    }//GEN-LAST:event_btnEliminar_adyacenteActionPerformed

/*    private void grafoloco(){
       for (int i = 65; i < (65+100); i++) {
             Character c=new Character((char)i);
            txtNombre.setText(c.toString());
            btnAgregar_VerticeActionPerformed(null);
        }
       grafo.allVerWithAll();
        grafo.dibujarInmediato=true;
        grafo.updateGraphics();
        grafo.repaint();
    }*/
    
    private void formWindowOpened(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowOpened
        grafo = new Grafo(areagrafica);
        try {
            grafo.archivo_cargar("grafo.bs");
            menucuadricula.setSelected(grafo.isDibujarCuadricula());
        } catch (IOException ex) {

        }
    }//GEN-LAST:event_formWindowOpened

    private void areagraficaMouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_areagraficaMouseReleased
       // System.out.println("terminado arrastre"+evt.getButton());
        grafo.interfaz_terminarArrastre(evt.getX(), evt.getY(),evt.getButton());
    }//GEN-LAST:event_areagraficaMouseReleased
    private void areagraficaMouseMoved(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_areagraficaMouseMoved
        try {
            grafo.setPesoGrafico(Integer.parseInt(txtPeso.getText()));
        } catch (Exception e) {
        }
        if (grafo!=null) txtinfoV.setText(grafo.interfaz_movimientoMouse(evt.getX(), evt.getY()));
    }//GEN-LAST:event_areagraficaMouseMoved
    private void areagraficaMouseDragged(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_areagraficaMouseDragged
//        System.out.println("arrastrando "+evt.getButton());
        try {
            grafo.setPesoGrafico(Integer.parseInt(txtPeso.getText()));
        } catch (Exception e) {
        }
        grafo.interfaz_arrastre(evt.getX(), evt.getY(),menuLimitar.isSelected());
    }//GEN-LAST:event_areagraficaMouseDragged
    private void areagraficaMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_areagraficaMousePressed
     //   System.out.println("click o inicio arrastre"+evt.getButton());
        grafo.interfaz_click_o_iniciarArrastre(evt.getX(), evt.getY(),evt.getButton());
        btnorigen.requestFocusInWindow();
    }//GEN-LAST:event_areagraficaMousePressed
    private void menuadyacenciaunoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuadyacenciaunoActionPerformed
        grafo.allVerWithAll();
    }//GEN-LAST:event_menuadyacenciaunoActionPerformed

    private void jMenuItem1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem1ActionPerformed
System.exit(0);
    }//GEN-LAST:event_jMenuItem1ActionPerformed

    private void menuVrecorridosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuVrecorridosActionPerformed
        if (frecorridos==null ){
            frecorridos=new f2(this);
            frecorridos.setVisible(true);
            frecorridos.setBounds(this.getWidth()+this.getX()-frecorridos.getWidth(), this.getY(),frecorridos.getWidth(), frecorridos.getHeight());
        }
    }//GEN-LAST:event_menuVrecorridosActionPerformed

    private void txtNombreKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtNombreKeyPressed
        if (evt.getKeyCode()==10){
            btnAgregar_VerticeActionPerformed(null);
            txtNombre.requestFocus();
            txtNombre.selectAll();
        }
    }//GEN-LAST:event_txtNombreKeyPressed

    private void txtVer_FinKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtVer_FinKeyPressed
        if (evt.getKeyCode()==10){
            btnAgregar_AdyActionPerformed(null);
            txtVer_Fin.requestFocus();
            txtVer_Fin.selectAll();
        }
    }//GEN-LAST:event_txtVer_FinKeyPressed

    private void txtNuevoNombreKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtNuevoNombreKeyPressed

        if (evt.getKeyCode()==10){
            btnModificar_verticeActionPerformed(null);
        }
    }//GEN-LAST:event_txtNuevoNombreKeyPressed

    private void txtVer_FinActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtVer_FinActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtVer_FinActionPerformed

    private void formWindowClosed(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowClosed
    }//GEN-LAST:event_formWindowClosed

    private void formWindowClosing(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowClosing
        try {
            grafo.archivo_guardar("grafo.bs");
        } catch (IOException ex) {
        }

    }//GEN-LAST:event_formWindowClosing

    private void menueliminarVerticesActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menueliminarVerticesActionPerformed
        grafo.eliminar_vertices();
        grafo.updateGraphics();
        grafo.repaint();
    }//GEN-LAST:event_menueliminarVerticesActionPerformed

    private void menuEliminarAdysActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuEliminarAdysActionPerformed
        grafo.eliminar_adyacencias();
        chkmostrarExpansion.setSelected(false);
        grafo.updateGraphics();
        grafo.repaint();
    }//GEN-LAST:event_menuEliminarAdysActionPerformed

    private void checkdirigidoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_checkdirigidoActionPerformed
        grafo.setDibujarDirigido(checkdirigido.isSelected());
    }//GEN-LAST:event_checkdirigidoActionPerformed

    private void btnorigenKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_btnorigenKeyPressed
        grafo.interfaz_teclaPulsada(evt.getKeyCode());
    }//GEN-LAST:event_btnorigenKeyPressed

    private void btnorigenActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnorigenActionPerformed
        grafo.setCi(new Point(0, 0));
    }//GEN-LAST:event_btnorigenActionPerformed

    private void txtNombreActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtNombreActionPerformed

    }//GEN-LAST:event_txtNombreActionPerformed

    private void txtNuevoNombreKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtNuevoNombreKeyReleased

    }//GEN-LAST:event_txtNuevoNombreKeyReleased

    private void txtNombreKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtNombreKeyReleased
        //grafo.centrar_vertice(txtNombre.getText());
    }//GEN-LAST:event_txtNombreKeyReleased

    private void mnuorganizarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mnuorganizarActionPerformed
        grafo.organizar_anchura_visual(txtNombre.getText());
        txtNombre.requestFocusInWindow();
        txtNombre.selectAll();
    }//GEN-LAST:event_mnuorganizarActionPerformed

    private void jMenu2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenu2ActionPerformed
    
    }//GEN-LAST:event_jMenu2ActionPerformed

    private void menucuadriculaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menucuadriculaActionPerformed
        grafo.setDibujarCuadricula(!grafo.isDibujarCuadricula());
        menucuadricula.setSelected(grafo.isDibujarCuadricula());

    }//GEN-LAST:event_menucuadriculaActionPerformed

    private void areagraficaMouseWheelMoved(java.awt.event.MouseWheelEvent evt) {//GEN-FIRST:event_areagraficaMouseWheelMoved
        System.out.println("la madre");
    }//GEN-LAST:event_areagraficaMouseWheelMoved

    private void areagraficaKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_areagraficaKeyPressed

    }//GEN-LAST:event_areagraficaKeyPressed

    private void btnorigenKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_btnorigenKeyTyped
        grafo.interfaz_teclaPulsada(evt.getKeyCode());
    }//GEN-LAST:event_btnorigenKeyTyped

    private void formWindowActivated(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowActivated
    }//GEN-LAST:event_formWindowActivated

    private void formComponentResized(java.awt.event.ComponentEvent evt) {//GEN-FIRST:event_formComponentResized
        if (this.getWidth()<= warea+areagrafica.getX()){
            this.setSize(warea+areagrafica.getX()+50, this.getHeight());
//            areagrafica.setSize(this.getWidth()-areagrafica.getX()-100, areagrafica.getHeight());
        }else if (this.getWidth()>=areagrafica.getX()+areagrafica.getWidth()+50){
            areagrafica.setSize(this.getWidth()-areagrafica.getX()-50, areagrafica.getHeight());
        }
        if (this.getHeight()<= harea+areagrafica.getY()){
            this.setSize(this.getWidth(),harea+areagrafica.getY()+100);
        }else if (this.getHeight()>=areagrafica.getY()+areagrafica.getHeight()+100){
            areagrafica.setSize(areagrafica.getWidth(),this.getHeight()-areagrafica.getY()-100);
        }
        if (grafo!=null){
            grafo.setAreaGrafica(areagrafica);
        }
    }//GEN-LAST:event_formComponentResized

    private void txtPesoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtPesoActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtPesoActionPerformed

    private void menuInformacionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuInformacionActionPerformed
        if (finfo==null){
            finfo=new f4(this);
            finfo.setVisible(true);
        }
    }//GEN-LAST:event_menuInformacionActionPerformed

    private void chkpesorRealActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_chkpesorRealActionPerformed
        txtPeso.setEnabled(!chkpesorReal.isSelected());
        grafo.setPesoReal(chkpesorReal.isSelected());
    }//GEN-LAST:event_chkpesorRealActionPerformed

    private void chkmostrarExpansionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_chkmostrarExpansionActionPerformed
        panelExpansion.setEnabled(chkmostrarExpansion.isSelected());
        grafo.setVisibleExpansion(chkmostrarExpansion.isSelected());
    }//GEN-LAST:event_chkmostrarExpansionActionPerformed

    private void chksoloexpansionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_chksoloexpansionActionPerformed
        grafo.setMostrarSoloExpansion(chksoloexpansion.isSelected());
    }//GEN-LAST:event_chksoloexpansionActionPerformed

    private void opcionKruskalActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_opcionKruskalActionPerformed
        grafo.setMetodoExpansion(Grafo.expansionMinima.Kruskal);
    }//GEN-LAST:event_opcionKruskalActionPerformed

    private void opcionPrimActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_opcionPrimActionPerformed
        grafo.setMetodoExpansion(Grafo.expansionMinima.Prim);
    }//GEN-LAST:event_opcionPrimActionPerformed

    private void jMenuItem2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem2ActionPerformed
               new f3().setVisible(true);
         // TODO add your handling code here:
    }//GEN-LAST:event_jMenuItem2ActionPerformed

    private void btnIniciarAnalisisThomsonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnIniciarAnalisisThomsonActionPerformed
        AFDOptimoEdgar.iniciarAnalisisGrafoPorThomson();
    }//GEN-LAST:event_btnIniciarAnalisisThomsonActionPerformed
   
    /**
    * @param args the command line arguments
    */
    public static void main(String args[]) throws UnsupportedLookAndFeelException {
                
        try {
            WindowsLookAndFeel tema=new WindowsLookAndFeel();
            UIManager.setLookAndFeel(tema);
        } catch (Exception e) {
        }
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Implementacion_Grafo().setVisible(true);
               // new f3().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel areagrafica;
    private javax.swing.JButton btnAgregar_Ady;
    private javax.swing.JButton btnAgregar_Vertice;
    private javax.swing.JButton btnEliminar_adyacente;
    private javax.swing.JButton btnEliminar_vertice;
    private javax.swing.JButton btnIniciarAnalisisThomson;
    private javax.swing.JButton btnModificar_adyacente;
    private javax.swing.JButton btnModificar_vertice;
    private javax.swing.JButton btnorigen;
    private javax.swing.JCheckBox checkdirigido;
    private javax.swing.JCheckBox chkmostrarExpansion;
    private javax.swing.JCheckBox chkpesorReal;
    private javax.swing.JCheckBox chksoloexpansion;
    private javax.swing.ButtonGroup grupoExpansion;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JMenu jMenu1;
    private javax.swing.JMenu jMenu2;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JMenuItem jMenuItem1;
    private javax.swing.JMenuItem jMenuItem2;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JSeparator jSeparator2;
    private javax.swing.JSeparator jSeparator3;
    private javax.swing.JPopupMenu.Separator jSeparator4;
    private javax.swing.JMenuItem menuEliminarAdys;
    private javax.swing.JMenuItem menuInformacion;
    private javax.swing.JCheckBoxMenuItem menuLimitar;
    private javax.swing.JMenuItem menuVrecorridos;
    private javax.swing.JMenuItem menuadyacenciauno;
    private javax.swing.JCheckBoxMenuItem menucuadricula;
    private javax.swing.JMenuItem menueliminarVertices;
    private javax.swing.JMenuItem mnuorganizar;
    private javax.swing.JRadioButton opcionKruskal;
    private javax.swing.JRadioButton opcionPrim;
    private javax.swing.JPanel panelExpansion;
    private javax.swing.JDialog sabiasque;
    public javax.swing.JTextField txtNombre;
    public javax.swing.JTextField txtNuevoNombre;
    private javax.swing.JTextField txtPeso;
    public javax.swing.JTextField txtVer_Fin;
    private javax.swing.JTextArea txtinfoV;
    // End of variables declaration//GEN-END:variables

}
