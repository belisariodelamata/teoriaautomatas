package clases;

import java.awt.Rectangle;

public class Vertice extends Rectangle {

    static boolean adyacenciaPesoReal = false;
    static boolean actualizarPesoReal = true;
    ///////////////////////////////
    String nomVer;
    protected int num_ent;
    protected int num_sal;
    Vertice sig;
    Adyacente lista_ady;//lista a donde se dirigen
    Adyacente lista_lle;//lista de los que llegan

    public Vertice(Vertice vertice) {
        //se crea un vertice a partir de las caracteristicas visuales de otro vertice
        this.nomVer = vertice.nomVer;
        this.num_ent = 0;
        this.num_sal = 0;
        this.sig = null;
        this.lista_ady = null;
        this.lista_lle = null;
        super.x = (int) vertice.getX();
        super.y = (int) vertice.getY();
        this.width = vertice.width;
        this.height = vertice.height;
    }

    public Vertice(String nombre) {
        this.nomVer = nombre;
        this.num_ent = 0;
        this.num_sal = 0;
        this.sig = null;
        this.lista_ady = null;
        this.lista_lle = null;
    }

    public boolean sinRelaciones() {
        if (num_ent == 0 && num_sal == 0) {
            return true;
        }
        return false;
    }

    //Inserta un aycente al final de la lista de adyacentes
    public boolean agregar_ady(Vertice ver_final, double peso, boolean dirigido) {
        if (ver_final == this) {
            return false;//no se puede enlazar el vertice con el mismo...no por ahora
        }
        if (isInAdyacentes(ver_final)) {
            return false;//si este vertice ya se encuentra en los adyacentes sale del proc
        }
        if (adyacenciaPesoReal) {
            peso = pesoReal(ver_final);
        }
        boolean agregado = false;
        Adyacente q = new Adyacente(ver_final, peso, dirigido);//se crea el adyacente
        if (this.lista_ady == null) {//si no hay lista de adyacentes
            this.lista_ady = q;//este es el primer adyacente
            ver_final.num_ent++;
            this.num_sal++;
            q.vertice.agregar_llegado(this, peso);
            agregado = true;
        } else {
            //Ubicarse al final de la lista de adyacentes
            Adyacente ult = this.lista_ady;
            while (ult.sig != null) {
                ult = ult.sig;
            }
            ///////////////
            ult.sig = q; //Unimos el último nodo con el nuevo nodo
            q.vertice.agregar_llegado(this, peso);
            ver_final.num_ent++;
            this.num_sal++;
            agregado = true;
        }
        if (agregado) {
            Adyacente bus = ver_final.buscar_ady(this.nomVer);//busca si se encuentra este vertice en el
            //vertice final
            //si es asi..entonces el adyacente se convierte en no dirigido, es decir, doble via
            if (bus != null) {
                q.dirigido = false;
                bus.dirigido = false;
            } else if (!dirigido) {
                //se debe añadir este Vertice a la lista de adyacencia del vertice final;
                ver_final.agregar_ady(this, peso, dirigido);
            }
        }
        return agregado;
    }

    public boolean agregar_ady(Vertice ver_final, String transiciones) {
        boolean dirigido = true;
        boolean agregado = false;
        Adyacente q = new Adyacente(ver_final, transiciones);//se crea el adyacente
        if (this.lista_ady == null) {//si no hay lista de adyacentes
            this.lista_ady = q;//este es el primer adyacente
            ver_final.num_ent++;
            this.num_sal++;
            //q.vertice.agregar_llegado(this, peso);
            agregado = true;
        } else {
            //Ubicarse al final de la lista de adyacentes
            Adyacente ult = this.lista_ady;
            while (ult.sig != null) {
                ult = ult.sig;
            }
            ///////////////
            ult.sig = q; //Unimos el último nodo con el nuevo nodo
            //q.vertice.agregar_llegado(this, peso);
            ver_final.num_ent++;
            this.num_sal++;
            agregado = true;
        }
        if (agregado) {
            Adyacente bus = ver_final.buscar_ady(this.nomVer);//busca si se encuentra este vertice en el
            //vertice final
            //si es asi..entonces el adyacente se convierte en no dirigido, es decir, doble via
            if (bus != null) {
                q.dirigido = false;
                bus.dirigido = false;
            } else if (!dirigido) {
                //se debe añadir este Vertice a la lista de adyacencia del vertice final;
                ver_final.agregar_ady(this, transiciones);
            }
        }
        return agregado;
    }

    public boolean eliminar_ady(String nomAdy) {
        boolean eliminado = false;
        Adyacente adyacenteliminado = null;//guardo el adyacente que elimino
        if (lista_ady != null) {
            if (lista_ady.vertice.nomVer.equals(nomAdy) && lista_ady.sig == null) {
                adyacenteliminado = lista_ady;
                num_sal--;
                lista_ady.vertice.num_ent--;
                lista_ady = null;
                eliminado = true;
            } else if (lista_ady.vertice.nomVer.equals(nomAdy) && lista_ady.sig != null) {
                adyacenteliminado = lista_ady;
                num_sal--;
                lista_ady.vertice.num_ent--;
                lista_ady = lista_ady.sig;
                eliminado = true;
            } else {
                Adyacente ant = lista_ady;
                Adyacente act = lista_ady.sig;
                while (act != null) {
                    if (act.vertice.nomVer.equals(nomAdy)) {
                        adyacenteliminado = act;
                        num_sal--;
                        act.vertice.num_ent--;
                        ant.sig = act.sig;
                        eliminado = true;
                        break;
                    }
                    ant = act;
                    act = act.sig;
                }
            }
            if (eliminado) {
                adyacenteliminado.vertice.eliminar_llegado(this);//eliminamos este vertice de la lista
                if (!adyacenteliminado.dirigido) {//si este adyacente es dirigido
                    adyacenteliminado.vertice.eliminar_ady(this.nomVer);
                }
                //de llegados del vertice eliminado de esta lista de adyacentes
            }
        }
        return eliminado;
    }

    private boolean agregar_llegado(Vertice ver_inicio, double peso) {
        Adyacente q = new Adyacente(ver_inicio, peso, false);
        if (this.lista_lle == null) {
            this.lista_lle = q;
            return true;
        } else {
            //Ubicarse al final de la lista de adyacentes
            Adyacente ult = this.lista_lle;
            while (ult.sig != null) {
                if (ult.vertice == ver_inicio) {
                    return false;//existe en la lista
                }
                ult = ult.sig;
            }
            if (ult.vertice == ver_inicio) {
                return false;//existe en la lista
            }
            ult.sig = q; //Unimos el último nodo con el nuevo nodo
        }
        return false;
    }

    private boolean eliminar_llegado(Vertice ver_inicio) {
        if (this.lista_lle != null) {
            if (lista_lle.vertice == ver_inicio && lista_lle.sig == null) {
                lista_lle = null;
                return true;
            } else if (lista_lle.vertice == ver_inicio && lista_lle.sig != null) {
                lista_lle = lista_lle.sig;
                return true;
            } else {
                Adyacente ant = lista_lle;
                Adyacente act = lista_lle.sig;
                while (act != null) {
                    if (act.vertice == ver_inicio) {
                        ant.sig = act.sig;
                        return true;
                    }
                    ant = act;
                    act = act.sig;
                }
            }
        }
        return false;
    }

    public boolean quitar_Relaciones() {
        Adyacente ady = lista_ady;
        while (ady != null) {
            eliminar_ady(ady.vertice.nomVer);
            ady = ady.sig;
        }
        ady = lista_lle;
        while (ady != null) {
            eliminar_llegado(ady.vertice);
            ady.vertice.eliminar_ady(this.nomVer);
            ady = ady.sig;
        }
        return false;
    }

    public boolean isInAdyacentes(Vertice v) {
        if (lista_ady != null) {
            Adyacente ady = lista_ady;
            while (ady != null) {
                if (ady.vertice == v) {
                    return true;
                }
                ady = ady.sig;
            }
        }
        return false;
    }

    public Adyacente buscar_ady(String nombre) {
        if (lista_ady != null) {
            Adyacente ady = lista_ady;
            while (ady != null) {
                if (ady.vertice.nomVer.equals(nombre)) {
                    return ady;
                }
                ady = ady.sig;
            }
        }
        return null;
    }

    public boolean modificar_ady(String nombre, String nnombre) {
        Adyacente ba = buscar_ady(nombre);
        if (ba != null) {
            ba.vertice.nomVer = nnombre;
            return true;
        }
        return false;
    }

    @Override
    public String toString() {
        String t = "Ver: " + nomVer + " GE: " + num_ent + " GS: " + num_sal + "\n";
        Adyacente p = this.lista_ady;
        while (p != null) {
            t += " Ady:" + p.toString() + "\n";
            p = p.sig;
        }
        return t;
    }

    public double pesoReal(Vertice ver_final) {
        return Math.sqrt(Math.pow(this.getCenterX() - ver_final.getCenterX(), 2) + Math.pow(this.getCenterY() - ver_final.getCenterY(), 2));
    }

    public void actualizarPesos() {
        Adyacente ady = lista_lle;
        while (ady != null) {
            Adyacente ady2 = ady.vertice.buscar_ady(this.nomVer);
            if (ady2 != null) {
                ady2.peso = pesoReal(ady.vertice);
            }
            ady = ady.sig;
        }
        ady = lista_ady;
        while (ady != null) {
            ady.peso = pesoReal(ady.vertice);
            ady = ady.sig;
        }
    }

    public void setX(int x) {
        super.x = x;
        if (adyacenciaPesoReal && actualizarPesoReal) {
            actualizarPesos();
        }
//        this.x = x;
    }

    public void setY(int y) {
        super.y = y;
        if (adyacenciaPesoReal && actualizarPesoReal) {
            actualizarPesos();
        }
        //        this.y = y;
    }

    public void setXY(int x, int y) {
        super.x = x;
        super.y = y;
        if (adyacenciaPesoReal && actualizarPesoReal) {
            actualizarPesos();
        }
        //        this.x=x;this.y=y;
    }

    public void setAddXY(int x, int y) {
        super.x += x;
        super.y += y;
        if (adyacenciaPesoReal && actualizarPesoReal) {
            actualizarPesos();
        }
        //        this.x+=x;this.y+=y;
    }

}
