package clases;

import java.util.Arrays;
import java.util.LinkedHashSet;
import java.util.Set;
import java.util.stream.Collectors;

public class Adyacente {

    Vertice vertice;//cambio en la clase para el manejo de punteros de memoria
    Set<String> transiciones = new LinkedHashSet<String>();
    double peso;//el peso es de este tipo para acercarlo a la realidad....
    Adyacente sig;
    boolean dirigido = true;

    public Adyacente(Vertice nombre, String transiciones) {
        String[] vectorTransicion = transiciones.split(" ");
        this.transiciones.addAll(Arrays.asList(vectorTransicion));
    }

    public Adyacente(Vertice nombre, double peso) {
        this.vertice = nombre;
        this.peso = peso;
        this.sig = null;
    }

    public Adyacente(Vertice nombre, double peso, boolean dirigido) {
        this.vertice = nombre;
        this.peso = peso;
        this.sig = null;
        this.dirigido = dirigido;
    }

    @Override
    public String toString() {
        return transiciones.stream().collect(Collectors.joining(" ")) + "→" + vertice.nomVer;
    }
}
