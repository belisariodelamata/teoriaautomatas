/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package automatas20172;

import extras.BsList;
import java.awt.Point;

import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public class Grafo {

    final Set<Vertice> vertices = new LinkedHashSet<>();
//    Vertice cab;
//    Vertice fin;
    private final String sepDato = "@";
    private final String sepDato2 = "*";
    BsList listEtiquetas = new BsList();//resultado de la lista de etiquetas generada por dijkstra
    // private BsBuilderGraph builder = null;
    BsList objetosrevisado = null;//lista de objetos comprobados

    private boolean visibleExpansion = false;//establece si se muestra la expansion minima

    /**
     * Añade al conjunto de Vertices, todas los verticiones que tienen
     * recursivamente transiciones lamdas
     *
     * @param verticesEvaluar
     */
    void expandirLambdas(Set<Vertice> verticesEvaluar) {
        Set<Vertice> verticesEvaluarTmp = verticesEvaluar;
        int numeroVertices;
        do {
            Set<Vertice> verticesNuevosDetectados = new LinkedHashSet<>();
            numeroVertices = verticesEvaluar.size();
            verticesEvaluarTmp.forEach(
                    (vertice) -> {
                        verticesNuevosDetectados.addAll(vertice.verticesConTransicionLambda());
                    }
            );
            verticesEvaluar.addAll(verticesNuevosDetectados);
            verticesEvaluarTmp = verticesNuevosDetectados;
            ///Si se añadieron nuevo vertices, se deben seguir evaluando las transciones
            ///lamdas de los mismos
        } while (numeroVertices != verticesEvaluar.size());
    }

    boolean evaluarCadena(String cadenaEvaluar) {
        boolean evaluacionExitosa = false;
        ///Se crea un conjunto de todos los vertices que son iniciales
        ///en teoría debe ser uno solo pero se le dará tolerancia para permitir más de uno.
        Set<Vertice> verticesEvaluar = vertices.stream().filter(v -> v.isvInicial()).collect(Collectors.toSet());
        ///Es posible que los vertices iniciales tengan unas transiciones lamdas
        ///por lo tanto dichos vertices tambien harán parte del conjunto inicial
        expandirLambdas(verticesEvaluar);
        ///
        boolean abortar=false;
        int i = -1;
        do {
            i++;
            if (i == cadenaEvaluar.length()) {///Si la iteracion actual corresponde a la final de la cadena
                ///Hay que verificar los vertices actuales, y en caso que alguno sea final, entonces
                ///la cadena es valida
                if (verticesEvaluar.stream().filter(v -> v.isvFinal()).count() > 0) {
                    evaluacionExitosa = true;
                }
                abortar=true;
            } else {
                String caracterAEvaluar = cadenaEvaluar.substring(i, i + 1);
                Set<Vertice> verticesTmp = new LinkedHashSet<>();
                verticesEvaluar.forEach(
                        (vertice) -> {
                            vertice.lista_ady.stream().forEach(
                                    (adyacente) -> {
                                        adyacente.transiciones.stream().forEach(
                                                (transicion) -> {
                                                    if (transicion.equals(caracterAEvaluar)) {
                                                        verticesTmp.add(adyacente.getVertice());
                                                    }
                                                }
                                        );
                                    }
                            );
                        }
                );
                ///Si no hay vertices con transiciones validas, entonces la cadena no es valida
                if (verticesTmp.isEmpty()){
                    abortar=true;
                }else{
                    expandirLambdas(verticesTmp);
                    verticesEvaluar=verticesTmp;
                }
            }
        } while (!abortar);
        return evaluacionExitosa;
    }

    public enum expansionMinima {

        Kruskal, Prim
    };
    private expansionMinima metodoExpansion = expansionMinima.Kruskal;

    private boolean proceso = false;//es para indicar que se esta realizando un proceso
    //y no realizar continuas actualizaciones cuando este se esté ejecutando, sino cuando termine

    public boolean isVacio() {
        return vertices.isEmpty();
    }

    public Grafo(javax.swing.JPanel contenedor) {
//        builder = new BsBuilderGraph(contenedor, this);
    }

    public void setAreaGrafica(javax.swing.JPanel contenedor) {
//        if (builder != null) {
//            builder.setArea(contenedor);
//        }
    }

    public Grafo() {
//        builder = null;
    }

    public void eliminar_vertices() {
        this.vertices.clear();
//        cab = null;
//        fin = null;
//        if (builder != null) {
//            builder.setExpansionMinima(this.getKruskal());
//            builder.setCi(new Point(0, 0));
//        }
    }

    public void eliminar_adyacencias() {
        setVisibleExpansion(false);
        vertices.forEach((Vertice v) -> v.quitar_Relaciones());
    }

    private boolean isNombre_vertice_valido(String nombre) {
        if ((nombre.trim().equals("") | nombre.equals(sepDato)
                | nombre.equals(sepDato2))) {
            return false;
        }
        return true;
    }

    public boolean agregar_vertices(String nombres) {
        String inicios[] = nombres.trim().split(",");

        for (int i = 0; i < inicios.length; i++) {
            if (isNombre_vertice_valido(inicios[i]) == false || buscar_vertice(inicios[i]) != null) {
                continue;
            }
            Vertice q = new Vertice(inicios[i]);
            vertices.add(q);
//            if (cab == null) {
//                cab = q;
//                fin = q;
//            } else {
//                fin.sig = q;
//                fin = q;
//            }
//            if (builder != null) {
//                builder.agregar_vertice_grafico(q);
//            }
        }
//        if (builder != null) {
//            builder.updateAndRepaint();
//        }
        return true;
    }

    public List<Vertice> agregarVertice(String nombres) {
        return agregarVertice(nombres, false, false);
    }

    public List<Vertice> agregarVertice(String nombres, boolean esInicial, boolean esFinal) {
        List<Vertice> verticesCreados = new ArrayList();
        String inicios[] = nombres.trim().split(",");

        for (int i = 0; i < inicios.length; i++) {
            if (isNombre_vertice_valido(inicios[i]) == false || buscar_vertice(inicios[i]) != null) {
                continue;
            }
            Vertice q = new Vertice(esInicial, esFinal, inicios[i]);
            verticesCreados.add(q);
//            if (builder != null) {
//                builder.agregar_vertice_grafico(q);
//            }
        }
//        if (builder != null) {
//            builder.updateAndRepaint();
//        }
        return verticesCreados;
    }

    public boolean agregar_vertice_Caracteristico(Vertice vertice) {
        //se agrega un vertice a partir de las caracteristicas visuales de otro
        if (isNombre_vertice_valido(vertice.nomVer) == false || buscar_vertice(vertice.nomVer) != null) {
            return false;
        }
        Vertice q = new Vertice(vertice);
        vertices.add(q);
//        if (cab == null) {
//            cab = q;
//            fin = q;
//        } else {
//            fin.sig = q;
//            fin = q;
//        }
        return true;
    }

    private boolean agregar_vertice_y_adyacente(String ver_ini, String ver_final, double peso, boolean dirigido) {
        //es unico para crear grafos faciles, solo usado internamente por los procedimientos kruskal y prim
        //este procedimiento crea el adyacente y el vertice de inicio si no existe
        ver_final = ver_final.replace(',', " ".charAt(0));

        Vertice vi = buscar_vertice(ver_ini);
        if (vi == null) {//si no existe el vertice de inicio, entonces lo creamos
            agregar_vertices(ver_ini);
            vi = buscar_vertice(ver_ini);
        }
        Vertice vf = buscar_vertice(ver_final);//buscamos el vertice final
        if (vi != null) {
            if (vf == null) {
                if (agregar_vertices(ver_final) == false) {
                    return false;
                }
                vf = buscar_vertice(ver_final);
            }
            if (agregar_ady(vi, vf, peso, dirigido)) {
//                if (builder != null) {
//                    builder.agregar_adyacente_grafico(vi, vf, dirigido);
//                }
            }
            return true;
        }
        return false;
    }

    public boolean agregar_ady(Vertice ver_ini, Vertice ver_final, double peso, boolean dirigido) {
//        boolean valor = ver_ini.agregar_ady(ver_final, peso, dirigido);
//        if (!proceso) {//si no se está realizando un proceso de añadir varios adyacentes
//            if (builder != null) {//si tenemos un visor de grafo
//                if (isVisibleExpansion()) {//y se está mostrando el kruskal
//                    builder.setExpansionMinima(this.getKruskal());//refrescamos el kruskal grafico
//                }
//            }
//        }
//        return valor;
        return false;
    }

    public boolean agregar_ady(Vertice ver_ini, Vertice ver_final, String transiciones) {
        boolean valor = ver_ini.agregar_ady(ver_final, transiciones);
        if (!proceso) {//si no se está realizando un proceso de añadir varios adyacentes
//            if (builder != null) {//si tenemos un visor de grafo
//                if (isVisibleExpansion()) {//y se está mostrando el kruskal
//                    builder.setExpansionMinima(this.getKruskal());//refrescamos el kruskal grafico
//                }
//            }
        }
        return valor;
    }

    public boolean agregar_ady(String ver_ini, String ver_final, double peso, boolean dirigido) {
        //este procedimiento crea el adyacente si no existe
        ver_final = ver_final.replace(',', " ".charAt(0));
        Vertice vi = buscar_vertice(ver_ini);
        Vertice vf = buscar_vertice(ver_final);
        if (vi != null) {
            if (vf == null) {
                if (agregar_vertices(ver_final) == false) {
                    return false;
                }
                vf = buscar_vertice(ver_final);
            }
//            if (vi.agregar_ady(vf, peso,dirigido)) {
            if (agregar_ady(vi, vf, peso, dirigido)) {
//                if (builder != null) {
//                    if (isVisibleExpansion()) {
//                        //no se aceptan operaciones si esta visible kruskal
//                        builder.setExpansionMinima(this.getKruskal());
//                    } else {
//                        builder.agregar_adyacente_grafico(vi, vf, dirigido);
//                    }
//
//                }
            }
            return true;
        }
        return false;
    }

    public boolean agregar_ady(String ver_ini, boolean esInicial, String ver_final, boolean esFinal, String transiciones) {
        ver_ini = ver_ini.trim().toLowerCase();
        ver_final = ver_final.trim().toLowerCase();
        if (ver_ini.isEmpty() || ver_final.isEmpty()) {
            return false;
        }
        Vertice vi = obtenerVertice(ver_ini, esInicial, false);
        Vertice vf = obtenerVertice(ver_final, false, esFinal);
        agregar_ady(vi, vf, transiciones);
        return true;
    }

    public boolean agregar_ady(String ver_ini, String ver_final, String transiciones) {
        return agregar_ady(ver_ini, false, ver_final, false, transiciones);
    }

    public boolean eliminar_adyacente(String nomVer, String nomAdy) {
        boolean estado = false;
        Vertice ve = buscar_vertice(nomVer);
        if (ve != null) {
            estado = ve.eliminar_ady(nomAdy);
        }
        if (estado) {//si se eliminó algo
//            if (builder != null) {//y tenemos un visor
//                builder.listaResaltados.vaciar();//eliminados la lista de resaltados
//                if (isVisibleExpansion()) {//si estamos mostrando el kruskal
//                    //hay que actualizar el kruskal
//                    builder.setExpansionMinima(this.getKruskal());
//                }
//            }
        }
        return estado;

    }

    boolean eliminar_adyacente(Vertice nomVer, Vertice nomAdy) {
        boolean estado = nomVer.eliminar_ady(nomAdy.nomVer);
        if (estado) {//si se eliminó algo
//            if (builder != null) {//y tenemos un visor
//                builder.listaResaltados.vaciar();//eliminados la lista de resaltados
//                if (isVisibleExpansion()) {//si estamos mostrando el kruskal
//                    //hay que actualizar el kruskal
//                    builder.setExpansionMinima(this.getKruskal());
//                }
//            }
        }
        return estado;
    }

    public Vertice buscar_vertice(String ver_ini) {
        return vertices.stream().filter(v -> v.nomVer.equalsIgnoreCase(ver_ini)).findFirst().orElse(null);
//        Vertice p = cab;
//        while (p != null) {
//            if (p.nomVer.equals(ver_ini)) {
//                return p;
//            } else {
//                p = p.sig;
//            }
//        }
//        return null;
    }

    public Vertice obtenerVertice(String ver_ini, boolean esInicial, boolean esFinal) {
        Vertice p = vertices.stream().filter(v -> v.nomVer.equalsIgnoreCase(ver_ini)).findFirst().orElse(null);
        if (p == null) {
            p = agregarVertice(ver_ini, esInicial, esFinal).get(0);
            vertices.add(p);
        }
        return p;
    }

    public Vertice obtenerVertice(String ver_ini) {
        return obtenerVertice(ver_ini, false, false);
    }

    public boolean actualizar_vertice(String ver_ini, String nver_ini) {
        boolean estado = false;
//        Vertice bv = buscar_vertice(ver_ini);
//        nver_ini = nver_ini.replace(',', " ".charAt(0));
//        Vertice bf = buscar_vertice(nver_ini);
//        if (bv != null && bf == null && isNombre_vertice_valido(nver_ini)) {
//            bv.nomVer = nver_ini;
//            if (builder != null) {
//                builder.setTamano(bv);
//            }
//            estado = true;
//        }
//        if (estado && builder != null) {
//            if (isVisibleExpansion()) {
//                builder.setExpansionMinima(this.getKruskal());
//            }
//            builder.updateAndRepaint();
//        }
        return estado;
    }

    public boolean actualizar_adyacente(String nomVer, String nomAdy, String nuevoNomAdy) {
        boolean estado = false;
//        Vertice ve = buscar_vertice(nomVer);
//        if (ve != null) {
//            nuevoNomAdy = nuevoNomAdy.replace(',', " ".charAt(0));
//            if (isNombre_vertice_valido(nuevoNomAdy)) {
//                estado = ve.modificar_ady(nomAdy, nuevoNomAdy);
//            }
//        }
//        if (estado && builder != null) {
//            if (isVisibleExpansion()) {
//                builder.setExpansionMinima(this.getKruskal());
//            }
//            builder.updateAndRepaint();
//        }
        return estado;
    }

    public boolean eliminar_vertice(String nombre) {
//        boolean estado = false;
//        if (cab != null) {
//            if (cab.nomVer.equals(nombre) && cab.sig == null) {
//                cab.quitar_Relaciones();
//                cab = null;
//                fin = null;
//                estado = true;
//            } else if (cab.nomVer.equals(nombre) && cab.sig != null) {
//                cab.quitar_Relaciones();
//                cab = cab.sig;
//                estado = true;
//            } else {
//                Vertice ant = cab;
//                Vertice act = cab.sig;
//                while (act != null) {
//                    if (act.nomVer.equals(nombre)) {
//                        act.quitar_Relaciones();
//                        ant.sig = act.sig;
//                        if (act == fin) {
//                            fin = ant;
//                        }
//                        estado = true;
//                        break;
//                    }
//                    ant = act;
//                    act = act.sig;
//                }
//
//            }
//        }
//        if (estado && builder != null) {
//            builder.listaResaltados.vaciar();
//            if (isVisibleExpansion()) {
//                //hay que actualizar el kruskal
//                builder.setExpansionMinima(this.getKruskal());
//            }
//        }
        return false;
    }

    public boolean isVer2_ady_ver1(String nomver1, String nomver2) {
        Vertice bus = buscar_vertice(nomver1);
        if (bus != null) {
            Vertice bus2 = buscar_vertice(nomver2);
            if (bus2 != null) {
                return bus.isInAdyacentes(bus2);
            }
        }
        return false;
    }

    public String recorrido_profundidad(String ninicio) {
        Vertice inicio = buscar_vertice(ninicio);
        if (inicio == null) {
            return null;
        }
        String msg = "Recorrido en Profundidad de " + inicio.nomVer;
        objetosrevisado = new BsList();
        msg += "\n" + recorrido_profundidad(inicio);
        return msg;
    }

    private String recorrido_profundidad(Vertice inicio) {
//        String msg = null;
//        if (objetosrevisado.buscar(inicio) == null) {//SI EL OBJETO NO FUE REVISADO
//            objetosrevisado.insertar(inicio);//LO INSERTO EN LOS REVISADOS
//            msg = inicio.nomVer;//el mensaje actual es el nombre del vertice
//            Adyacente adyacente = inicio.lista_ady;//
//            while (adyacente != null) {//recorro la lista de adyacentes del vertice
//                String rec = recorrido_profundidad(adyacente.vertice);//y cada uno pasa a ser recorrido
//                if (rec != null) {
//                    msg += " , " + rec;
//                }
//                adyacente = adyacente.sig;
//            }
//        }
//        return msg;
        return null;
    }

//    public String getRutas() {
    //      return rutas;
    //}
    public String recorrido_anchura(String ninicio) {
//        Vertice inicio = buscar_vertice(ninicio);//realizo la busqueda del nombre del vertice pasado
//        if (inicio == null) {
//            return null;
//        }
//        String msg = null;
//        objetosrevisado = new BsList();
//        BsList listavertices = new BsList();//necesito una lista de vertices
//        listavertices.insertar(inicio);//convierto el Vertice actual en una lista de vertices
//        objetosrevisado.insertar(inicio);
//
//        Vertice inicio_arco = null;//variable que guarda el primer vertice del arco evaluado
//        //se entiene por arco la cantidad de aristas que se encuentra de un nodo inicial
//        while (!listavertices.isVacia()) {
//            Vertice va = (Vertice) listavertices.extraerPrimero().getObj();
//            //ordenar el mensaje
//            if (inicio_arco == va) {
//                msg += " | " + va.nomVer;
//                inicio_arco = null;
//            } else if (msg == null) {
//                msg = "| " + va.nomVer;
//            } else {
//                msg += " , " + va.nomVer;
//            }
//            ////////////////////
//            Adyacente ady = va.lista_ady;
//            while (ady != null) {
//                if (objetosrevisado.buscar(ady.vertice) == null) {
//                    objetosrevisado.insertar(ady.vertice);
//                    listavertices.insertar(ady.vertice);
//
//                    if (inicio_arco == null) {
//                        inicio_arco = ady.vertice;
//                    }
//                }
//                ady = ady.sig;
//            }
//        }
//        return "Recorrido en anchura de " + inicio.nomVer + "\n" + msg + " |";
        return null;
    }

    public String recorrido_topologico() {
//        BsList cola = new BsList();//creamos nuestra cola de vertices
//        BsList lista = new BsList(); //creamos nuestra li    sta de vertices
//        //que contendran los vertices pero con el numero de entradas modificado
//        Vertice q = cab;
//        String recorrido = "";
//        while (q != null) {//recorremos todos los vertices del grafo
//            //creamos un objeto que guardara el vertice con
//            //el numero de grados de entrada mofificado
//            //aunque la clase es Adyacente, no tiene nada que ver con ella
//            //solo es para usar la propiedad de peso que realmente
//            //sera el numero de grados de entrada
////            Adyacente nuevo_vertice = new Adyacente(q, q.num_ent);
//            ///////////////////////////////////////////////////
//            lista.insertar(q, q.num_ent);//añadimos el nuevo vertice a la linked list
//            //guardamos en una cola todos los vertices que no tengan entrada
//            if (q.num_ent == 0) {
//                cola.insertar(q);
//            }
//            q = q.sig;
//        }
//        while (!cola.isVacia()) {
//            NBsList nl = cola.extraerPrimero();//obtenemos y eliminamos el vertice de la cola
//            Vertice nv = (Vertice) nl.getObj();
//            Adyacente adya = nv.lista_ady;//obtenemos la lista de adyacentes del anterior vertice
//            while (adya != null) {
//                //////debemos buscar cual es el numero de grado de entrada
//                ///del vertice adyacente actual
//                NBsList bus = lista.buscar(adya.vertice);
//                //en este momento ya tenemos el vertice con grado modificado
//                bus.setRec(bus.getRec() - 1);//le quitamos un grado de entrada;
//                if (bus.getRec() == 0) {
//                    cola.insertar(bus.getObj());
//                }
//                adya = adya.sig;
//            }
//            recorrido += nv.nomVer + " ";
//        }
//        return recorrido;
        return null;
    }

    public String dijkstra(String inicio, boolean porPeso) {
        Vertice vinicio = buscar_vertice(inicio);
        //si no existen el vertice, no hago el recorrdio
        if (vinicio == null) {
            return "ruta no encontrada";
        }
        return dijkstra(vinicio, porPeso);
    }

    //el ultimo parametro es el vertice al que se quiere llegar
    //lo utilizo para la visualizacion de un camino
    String dijkstra(Vertice inicio, boolean porPeso) {
//        BsList listRevisados = new BsList();//es la lista de los vertices ya revisados
//        listEtiquetas = new BsList();//es la lista que guardará las etiquetas
//        listEtiquetas.insertar(new Etiqueta(inicio, inicio, null, 0, inicio.nomVer));//inserto la etiqueta del vertice de inicio
//        int nr = 0;//es el numero de vertices revisados
//        NBsList neti = null;//nodo para hacer recorrido en la lista
//        Etiqueta etiqueta = null; //utilizada para el casting de los nodos etiquetas
//        do {
//            //debo buscar el menor recorrido de la lista de etiquetas
//            neti = listEtiquetas.getPtr();//obtengo la cabeza de las etiquetas para poder hacer el recorrido
//            NBsList netimenor = null;//tengo la etiqueta menor vacia
//            Etiqueta etimenor = null;//es la etiqueta de la linea anterior pero ya pasada por casting
//            while (neti != null) {
//                etiqueta = (Etiqueta) neti.getObj();//obtengo la etiqueta del recorrido
//                if (!etiqueta.revisado) {
//                    if (netimenor == null) {//si esta etiqueta no esta revisada y no hay etiqueta menor
//                        //quiere decir que como es la primera, entonces por ahora es el menor recorrido
//                        netimenor = neti;//nodo de etiqueta menor
//                        etimenor = etiqueta;//etiqueta menor
//                    } else if (etiqueta.recorrido < etimenor.recorrido) {//si esta etiqueta es menor
//                        netimenor = neti;
//                        etimenor = etiqueta;
//                    }
//                }
//                neti = neti.getSig();
//            }
//            //////En este momento ya tenemos la etiqueta con el menor recorrido///////////////////////////////////////////////////
//            //System.out.println("Menor="+etimenor.toString());
//            etimenor.revisado = true;//la marcamos como revisada
//            listRevisados.insertar(etimenor.vertice);//inserto el VERTICE, no la etiqueta, en la lista de revisados
//            nr++;//incremento el numero de revisados
////            netimenor.setObj(etimenor);//y modificamos el nodo que la contenia
//            Adyacente ady = etimenor.vertice.lista_ady;//obtengo la lista de adyacentes de vertice con etiqueta menor
//            while (ady != null) {//recorremos la lista de adyacentes del vertice de la etiqueta encontrada con menor recorrido
//                //hay que verificar si el vertice adyacente ya fue revisado, si lo fue, no se debe seguir
//                if (!listRevisados.contiene(ady.vertice)) {//si este vertice no ha sido revisado
//                    Etiqueta nuevaetiqueta = new Etiqueta(etimenor.principal, ady.vertice, etimenor.vertice,
//                            etimenor.recorrido + ((porPeso) ? ady.peso : 1), etimenor.ruta + "," + ady.vertice.nomVer);//
//                    //hay que buscar si la etiqueta de este vertice ha sido insertada previamente para verificar su recorrido
//                    neti = listEtiquetas.getPtr();//obtengo la cabeza de la lista de etiquetas para iniciar la iteracion o recorrido de lista
//                    boolean insertarEtiqueta = true;///Validamos si la nueva etiqueta la podemos ingresar a la lista
//                    while (neti != null) {
//                        etiqueta = (Etiqueta) neti.getObj();
//                        if (etiqueta.vertice == nuevaetiqueta.vertice) {//si es la etiqueta del mismo vertice
//                            //System.out.println("hacia "+etiqueta.vertice.nomVer);
//                            if (nuevaetiqueta.recorrido < etiqueta.recorrido) {//y la actual tiene un recorrido menor
//                                //System.out.println("Etiqueta sobreescrita"+nuevaetiqueta.recorrido+"<"+etiqueta.recorrido);
//                                neti.setObj(nuevaetiqueta);//la sobreescribimos
//                                insertarEtiqueta = false;
//                                ////si se encontró una menor entonces debo eliminar todas las etiquetas que tengan un recorrido en empate
//                                //sabemos que no es posible que sea la cabeza, porque siempre es el vertice de inicio
//                                NBsList naeliminar = neti.getSig();//creamos otra variable de recorrido o iteraccion
//                                while (naeliminar != null) {
//                                    Etiqueta etiEliminar = (Etiqueta) naeliminar.getObj();//obtengo la etiqueta del recorrido
//                                    if (etiEliminar.vertice == nuevaetiqueta.vertice) {//si encuentro otra etiqueta con el mismo vertice
//                                        nr++;//entonces como la voy a eliminar incremento el numero de revisados
//                                        //System.out.println("Eliminando ");
//                                        // System.out.println(etiEliminar.toString());
//                                        //JOptionPane.showMessageDialog(null, etiEliminar.toString());
//                                        naeliminar.getAnt().setSig(naeliminar.getSig());
//                                        if (naeliminar.getSig() != null) {
//                                            naeliminar.getSig().setAnt(naeliminar.getAnt());
//                                        } else {//si el elimado es el fin
//                                            listEtiquetas.setFin(naeliminar.getAnt());
//                                        }
//                                    }
//                                    naeliminar = naeliminar.getSig();
//                                }
//                            } else if (nuevaetiqueta.recorrido > etiqueta.recorrido) {
//                                //System.out.println("Etiqueta no incluyente:"+nuevaetiqueta.recorrido+">"+etiqueta.recorrido);
//                                insertarEtiqueta = false;
//                            } else if (nuevaetiqueta.recorrido == etiqueta.recorrido) {
//                                //System.out.println("Igual recorrido"+nuevaetiqueta.recorrido +","+etiqueta.recorrido);
//                                insertarEtiqueta = true;
//                            }
//                            break;
//                        }
//                        neti = neti.getSig();
//                    }
//                    if (insertarEtiqueta) {//sino encontro otra etiqueta insertamos la nueva etiqueta en la lista
//                        //System.out.println("Insertada " + nuevaetiqueta.toString() );
//                        listEtiquetas.insertar(nuevaetiqueta);
//                    }
//                }
//                ady = ady.sig;
//            }
//        } while (nr < listEtiquetas.getTamano());//mientras el numero de revisados sea menor a los insertados
//        listEtiquetas.extraerPrimero();//quito la etiqueta principal, ya que no quiero mostrarla
        return listEtiquetas.toString();
    }

    public String floyd(boolean porPeso) {
        String msg = "";
//        Vertice v = cab;
//        while (v != null) {
//            String dj = dijkstra(v.nomVer, porPeso);
//            msg += ((dj.equals("") ? "" : dj));
//            v = v.sig;
//        }
        return msg;
    }

    public Grafo getKruskal() {
        Grafo grafoKruskal = new Grafo();
//        if (cab != null) {
//            BsList listaAdyacentes = new BsList();
//            Vertice ncab = cab;
//            while (ncab != null) {
//                Adyacente ady = ncab.lista_ady;
//                while (ady != null) {
//                    listaAdyacentes.insertarPorOrdenRec(ncab, ady.vertice, ady.peso, ady.dirigido);
//                    ady = ady.sig;
//                }
//                ncab = ncab.sig;
//            }
//            while (!listaAdyacentes.isVacia()) {
//                NBsList ady = listaAdyacentes.extraerPrimero();
//                Vertice vinicio = (Vertice) ady.getObj();
//                Vertice vfin = (Vertice) ady.getObjrelacion();
//                if (!grafoKruskal.isVertice_RamaDe_impAnchura(vinicio, vfin, 1)) {
//                    if (!grafoKruskal.isVertice_RamaDe_impAnchura(vfin, vinicio, 1)) {
//                        grafoKruskal.agregar_vertice_y_adyacente(vinicio.nomVer, vfin.nomVer, ady.getRec(), ady.isEstado());
//                    }
//                }
//            }
//        }
        return grafoKruskal;
    }

    public Grafo getPrim() {
        Grafo grafoPrim = new Grafo();
//        BsList listaVisitados = new BsList();//es para no incluir nuevamente
//        //los adyacentes de los visitados, si llegan nuevamente como vertices finales
//        BsList listaAdyacentes = new BsList();
//        if (cab != null) {
//            listaAdyacentes.insertarPorOrdenRec(null, cab, 0, false);//inserto el grupo formado
//            //por el vertice inicial, que viene de ninguno, con recorrido cero, y no es dirigido
//            while (!listaAdyacentes.isVacia()) {
//                /////////////////////////
//                NBsList ady = listaAdyacentes.extraerPrimero();//devuelve siempre el primero
//                Vertice vinicio = (Vertice) ady.getObj();
//                Vertice vfin = (Vertice) ady.getObjrelacion();
//                /////////////////////////
//                if (!listaVisitados.contiene(vfin)) {
//                    boolean expandir = false;
//                    if (vinicio == null) {//LA PRIMERA VEZ QUE EXTRAIGA UN OBJETO,SERA EL INICIO NULO, SOLO HAY QUE AGREGAR EL FIN
//                        grafoPrim.agregar_vertices(vfin.nomVer);
//                        expandir = true;
//                    } else {
//                        listaVisitados.insertar(vfin);
//                        if (!grafoPrim.isVertice_RamaDe_impAnchura(vinicio, vfin, 1)) {
//                            if (!grafoPrim.isVertice_RamaDe_impAnchura(vfin, vinicio, 1)) {
//                                //if (vinicio==null){//esto quiere decir que estoy extrayendo el primero objeto..
//                                //es decir que solo voy a agregar el objerto vfin
//                                //     grafoPrim.agregar_vertices(vfin.nomVer);
//                                //}else{
//                                grafoPrim.agregar_vertice_y_adyacente(vinicio.nomVer, vfin.nomVer, ady.getRec(), ady.isEstado());
//                                //}
//                                expandir = true;
//                            }
//                        }
//                    }
//                    if (expandir) {
//                        Adyacente lady = vfin.lista_ady;
//                        while (lady != null) {
//                            listaAdyacentes.insertarPorOrdenRec(vfin, lady.vertice, lady.peso, lady.dirigido);
//                            lady = lady.sig;
//                        }
//                    }
//                }
//            }
//        }
        return grafoPrim;
    }

    public boolean isVertice_RamaDe_impAnchura(Vertice VerInicio, Vertice VertAEncontrar, int minRec) {
        ////////los vertices pasados en estos parametros, necesariamente no será parte de este mismo grafo...
        //por tanto solo seran necesarios sus nombres, para la correspondiente busqueda
        if (VerInicio == null || VertAEncontrar == null) {
            return false;
        }
        String nomVerInicio = VerInicio.nomVer, nomVertAEncontrar = VertAEncontrar.nomVer;
        //esta es una implementacion de anchura para saber si un vertice se encuentra en alguna rama de otro vertice
        //el parametro de MinRec indica cual es el minimo numero de arcos que debe haber recorrido
        //para devolver verdadero si se llega al "VerticeAEncontrar"
        Vertice inicio = buscar_vertice(nomVerInicio);//realizo la busqueda del nombre del vertice pasado
        if (inicio == null) {
            return false;
        }
        int numArcos = 0;//es la cantidad de arcos expandidos en el recorrido de anchura
        BsList listavertices = new BsList();//necesito una lista de vertices
        listavertices.insertar(inicio);//convierto el Vertice actual en una lista de vertices
        objetosrevisado = new BsList();
        objetosrevisado.insertar(inicio);
        Vertice inicio_arco = null;//variable que guarda el primer vertice del arco evaluado
        //se entiene por arco la cantidad de aristas que se encuentra de un nodo inicial
        while (!listavertices.isVacia()) {
            Vertice verticeExtraido = (Vertice) listavertices.extraerPrimero().getObj();
            if (inicio_arco == verticeExtraido) {
                inicio_arco = null;
                numArcos++;
            }
            ////////////////////
//            Adyacente ady = verticeExtraido.lista_ady;
//            while (ady != null) {
//                if (objetosrevisado.buscar(ady.vertice) == null) {
//                    if (ady.vertice.nomVer.equalsIgnoreCase(nomVertAEncontrar)) {//si en una rama esta el vertice a encontrar
//                        if (numArcos >= minRec) {//si el numero de arcos es mayor o igual al numero de arcos requeridos
//                            return true;
//                        }
//                    } else {
//                        objetosrevisado.insertar(ady.vertice);
//                        listavertices.insertar(ady.vertice);
//                    }
//                    if (inicio_arco == null) {
//                        inicio_arco = ady.vertice;
//                    }
//                }
//                ady = ady.sig;
//            }
        }
        return false;
    }

    @Override
    public String toString() {
        String t = "Contenido del Grafo: \n";
        t += vertices.stream().map(v -> v.toString()).collect(Collectors.joining(" "));
//        Vertice p = cab;
//        while (p != null) {
//            t += p.toString() + "\n";
//            p = p.sig;
//        }
        return t;
    }

    public void allVerWithAll() {
//        proceso = true;
//        if (cab != null) {
//            Vertice tcab = cab;
//            while (tcab != null) {
//                Vertice tact = cab;
//                while (tact != null) {
//                    if (tact != tcab) {
//                        agregar_ady(tcab, tact, 1, false);
//                    }
//                    tact = tact.sig;
//                }
//                tcab = tcab.sig;
//            }
//        }
//        proceso = false;
//        updateGraphics();
    }

    /**
     * **********************************************
     */
    //Guardar grafo
    public void archivo_guardar(String nomarchivo) throws IOException {
//        FileOutputStream archivo = null;
//        try {
//            archivo = new FileOutputStream(nomarchivo);
//            DataOutputStream escritura = new DataOutputStream(archivo);
//
//            escritura.writeBoolean(this.isVisibleExpansion());
//            escritura.writeBoolean(builder.dibujarCuadricula);
//            escritura.writeInt(builder.getCi().x);//coordenadas de inicio
//            escritura.writeInt(builder.getCi().y);
//            //System.out.println(ci.x +","+ci.y);
//            Vertice v = cab;
//            ///guardo los vertices
//            
//            while (v != null) {
//                escritura.writeUTF(v.nomVer);
//                escritura.writeInt((int) v.getX());
//                escritura.writeInt((int) v.getY());
//                // System.out.print(v.nomVer+"("+v.x+","+v.y+")");
//                v = v.sig;
//            }
//            escritura.writeUTF(sepDato);//termine de guardar los vertices
//            v = cab;
//            while (v != null) {
//                if (v.lista_ady != null) {
//                    escritura.writeUTF(v.nomVer);//guardo el vertice
//                    //System.out.println(v.nomVer);
//                    Adyacente ady = v.lista_ady;
//                    while (ady != null) {
//                        escritura.writeUTF(ady.vertice.nomVer);//con sus correspondientes adyacentes y pesos
//                        escritura.writeDouble(ady.peso);
//                        escritura.writeBoolean(ady.dirigido);
//                        ///System.out.println("|"+ady.nomAdy.nomVer+","+ady.peso+"|");
//                        ady = ady.sig;
//                    }
//                    escritura.writeUTF(sepDato2);//guardo el separador
//                    //System.out.println(sepDato2);
//                }
//
//                v = v.sig;
//            }
//            escritura.writeUTF(sepDato);
//            //System.out.println(sepDato);
//            escritura.close();
//        } catch (FileNotFoundException ex) {
//        } catch (IOException ioe) {
//        }
//        try {
//            archivo.close();
//        } catch (IOException ex) {
//
//        }
    }

    public void archivo_cargar(String nomarchivo) throws IOException {
//        builder.cargandoArchivo = true;
//        builder.dibujarInmediato = false;
//        FileInputStream archivo = null;
//        boolean tmpvisibleKruskal = false;
//        try {
//            archivo = new FileInputStream(nomarchivo);
//            DataInputStream lectura = new DataInputStream(archivo);
//            tmpvisibleKruskal = lectura.readBoolean();
//            builder.dibujarCuadricula = lectura.readBoolean();
//            Point tci = new Point(0, 0);
//            tci.x = lectura.readInt();//coordenadas de inicio
//            tci.y = lectura.readInt();//coordenadas de inicio
//
//            builder.setCi(new Point(tci));
//
//            cab = null;
//            fin = null;
//            ///guardo los vertices
//            String nombre = null;
//            while (true) {
//                nombre = lectura.readUTF();
//                if (nombre.equals(sepDato)) {
//                    break;//cuando obtenga el separador se detiene
//                }
//                Point coordenadasVertice = new Point();
//                coordenadasVertice.x = lectura.readInt();
//                coordenadasVertice.y = lectura.readInt();
//                builder.coordenadasVertice = coordenadasVertice;
//                agregar_vertices(nombre);
//            }
//            if (cab == null) {
//                archivo.close();
//            } else {
//                String nadya = null;
//                double padya = 0;
//                while (true) {
//                    nombre = lectura.readUTF();
//                    if (nombre.equals(sepDato)) {
//                        break;
//                    }
//                    while (true) {
//                        nadya = lectura.readUTF();
//                        if (nadya.equals(sepDato2)) {
//                            break;
//                        }
//                        padya = lectura.readDouble();
//                        boolean dir = lectura.readBoolean();
//                        agregar_ady(nombre, nadya, padya, dir);
//                    }
//
//                }
//                archivo.close();
//                if (isVisibleExpansion()) {
//                    builder.setExpansionMinima(this.getKruskal());
//                }
//            }
//        } catch (IOException ioe) {
//        }
//        try {
//            archivo.close();
//            if (isVisibleExpansion()) {
//                builder.setExpansionMinima(this.getKruskal());
//            }
//        } catch (NullPointerException npe) {
//        } catch (IOException ex) {
//
//        }
//        builder.dibujarInmediato = true;
//        builder.cargandoArchivo = false;
//        builder.updateGraphics();
//        builder.repaint();
    }
    //public boolean wisSiempreMostrarFlecha() {
    //return builder.siempremostrarflecha;
    //}
    //public void wsetSiempreMostrarFlecha(boolean siempreMostrarFlecha) {
    //this.builder.siempremostrarflecha = siempreMostrarFlecha;
    //}

    public boolean isDibujarDirigido() {
//        return builder.dibujarDirigido;
        return false;
    }

    public void setDibujarDirigido(boolean dibujarDirigido) {
//        this.builder.dibujarDirigido = dibujarDirigido;
    }

    public void repaint() {
//        builder.repaint();
    }

    public boolean isDibujarinmediato() {
//        return builder.dibujarInmediato;
        return false;
    }

    public void setDibujarinmediato(boolean dibujarinmediato) {
//        this.builder.dibujarInmediato = dibujarinmediato;
    }

    public void updateGraphics() {
//        builder.updateAndRepaint();
    }

    public boolean isDibujarCuadricula() {
//        return builder.dibujarCuadricula;
        return false;
    }

    public void setDibujarCuadricula(boolean dibujarCuadricula) {
//        this.builder.dibujarCuadricula = dibujarCuadricula;
        updateGraphics();
        repaint();
    }

    public void interfaz_terminarArrastre(int x, int y, int button) {
//        builder.interfaz_terminarArrastre(x, y, button);
    }

    public void interfaz_click_o_iniciarArrastre(int x, int y, int button) {
//        builder.interfaz_click_o_iniciarArrastre(x, y, button);
    }

    public void interfaz_arrastre(int x, int y, boolean limitar) {
//        builder.interfaz_arrastre(x, y, limitar);
    }

    public String interfaz_movimientoMouse(int x, int y) {
//        return builder.interfaz_movimientoMouse(x, y);
        return "";
    }

    public void interfaz_teclaPulsada(int tecla) {
//        builder.interfaz_teclaPulsada(tecla);
    }

    public void setCi(Point point) {
//        builder.setCi(point);
    }

    public void organizar_anchura_visual(String vertice) {
//        builder.organizar_por_anchura(vertice);
    }

    public void setPesoGrafico(int peso) {
//        if (peso > 0) {
//            builder.pesoAdyacenteGrafico = peso;
//        }
    }

    public boolean isVisibleExpansion() {
        return visibleExpansion;
    }

    public void setVisibleExpansion(boolean visibleExpansion) {
//        this.visibleExpansion = visibleExpansion;
//        if (builder != null) {
//            builder.setMetodoExpansion(metodoExpansion);
//            if (visibleExpansion) {
//                if (metodoExpansion == expansionMinima.Kruskal) {
//                    builder.colorExpansion = Color.RED;
//                    builder.setExpansionMinima(this.getKruskal());
//                } else {
//                    builder.colorExpansion = Color.orange;
//                    builder.setExpansionMinima(this.getPrim());
//                }
//
//            } else {
//                builder.setExpansionMinima(null);
//            }
//        }
    }

    public void setMostrarSoloExpansion(boolean mostrarSoloExpansion) {
//        if (builder != null) {
//            builder.setMostrarSoloExpansion(mostrarSoloExpansion);
//        }
    }

    public void setPesoReal(boolean estado) {
        Vertice.adyacenciaPesoReal = estado;
        if (estado) {
//            Vertice tmp = cab;
//            while (tmp != null) {
//                Adyacente ady = tmp.lista_ady;
//                while (ady != null) {
//                    ady.peso = tmp.pesoReal(ady.vertice);
//                    ady = ady.sig;
//                }
//                tmp = tmp.sig;
//            }
//            if (isVisibleExpansion() && builder != null) {
//                builder.setExpansionMinima(this.getKruskal());
//            }
//            updateGraphics();
        }
    }

    public expansionMinima getMetodoExpansion() {
        return metodoExpansion;
    }

    public void setMetodoExpansion(expansionMinima metodoExpansion) {
        this.metodoExpansion = metodoExpansion;
        setVisibleExpansion(visibleExpansion);

    }
}

class Etiqueta {

    Vertice principal = null;
    Vertice vertice = null;
    Vertice origen = null;
    double recorrido = 0;
    boolean revisado = false;
    String ruta = "";

    public Etiqueta(Vertice principal, Vertice vertice, Vertice origen, double recorrido, String ruta) {
        this.principal = principal;
        this.vertice = vertice;
        this.origen = origen;
        this.recorrido = recorrido;
        this.revisado = false;
        this.ruta = ruta;
    }

    @Override
    public String toString() {
//        return "De "+((origen==null)?"-":origen.nomVer)+"→"+vertice.nomVer+"**"+ruta+"="+recorrido;
//        return "De "+principal.nomVer+"→"+vertice.nomVer+"**"+ruta+"="+recorrido;
        return principal.nomVer + " → " + vertice.nomVer + " |" + ruta + "|  Peso " + (int) recorrido;
//A → B |A, B|  Peso 1
    }
}
