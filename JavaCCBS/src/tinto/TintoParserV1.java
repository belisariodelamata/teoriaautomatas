/* Generated By:JavaCC: Do not edit this line. TintoParserV1.java */
//------------------------------------------------------------------//
//                        COPYRIGHT NOTICE                          //
//------------------------------------------------------------------//
// Copyright (c) 2008, Francisco José Moreno Velo                   //
// All rights reserved.                                             //
//                                                                  //
// Redistribution and use in source and binary forms, with or       //
// without modification, are permitted provided that the following  //
// conditions are met:                                              //
//                                                                  //
// * Redistributions of source code must retain the above copyright //
//   notice, this list of conditions and the following disclaimer.  // 
//                                                                  //
// * Redistributions in binary form must reproduce the above        // 
//   copyright notice, this list of conditions and the following    // 
//   disclaimer in the documentation and/or other materials         // 
//   provided with the distribution.                                //
//                                                                  //
// * Neither the name of the University of Huelva nor the names of  //
//   its contributors may be used to endorse or promote products    //
//   derived from this software without specific prior written      // 
//   permission.                                                    //
//                                                                  //
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND           // 
// CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,      // 
// INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF         // 
// MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE         // 
// DISCLAIMED. IN NO EVENT SHALL THE COPRIGHT OWNER OR CONTRIBUTORS //
// BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,         // 
// EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED  //
// TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,    //
// DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND   // 
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT          //
// LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING   //
// IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF   //
// THE POSSIBILITY OF SUCH DAMAGE.                                  //
//------------------------------------------------------------------//

//------------------------------------------------------------------//
//                      Universidad de Huelva                       //
//          Departamento de Tecnologías de la Información           //
//   �?rea de Ciencias de la Computación e Inteligencia Artificial   //
//------------------------------------------------------------------//
//                     PROCESADORES DE LENGUAJE                     //
//------------------------------------------------------------------//
//                                                                  //
//          Compilador del lenguaje Tinto [Versión 0.1]             //
//                                                                  //
//------------------------------------------------------------------//


package tinto;

public class TintoParserV1 implements co.InterfaceAnalizador, TintoParserV1Constants {
    public void inicio() throws Exception{
        CompilationUnit();
    }
    public String textoLeido(){
        return "";
    }

/**************************************************************/
/*         EL AN�?LISIS DE LA CABECERA COMIENZA AQU�?           */
/**************************************************************/

/**
 * Reconoce el contenido completo de un archivo ".tinto"
 *
 * CompilationUnit -> (InportClause)* Library
 */
  final public void CompilationUnit() throws ParseException {
    label_1:
    while (true) {
      switch ((jj_ntk==-1)?jj_ntk():jj_ntk) {
      case IMPORT:
        ;
        break;
      default:
        jj_la1[0] = jj_gen;
        break label_1;
      }
      ImportClause();
    }
    Library();
  }

  final public void ImportClause() throws ParseException {
    jj_consume_token(IMPORT);
    jj_consume_token(IDENTIFIER);
    jj_consume_token(SEMICOLON);
  }

  final public void Library() throws ParseException {
    jj_consume_token(LIBRARY);
    jj_consume_token(IDENTIFIER);
    jj_consume_token(LBRACE);
    label_2:
    while (true) {
      switch ((jj_ntk==-1)?jj_ntk():jj_ntk) {
      case BOOLEAN:
      case CHAR:
      case INT:
      case VOID:
        ;
        break;
      default:
        jj_la1[1] = jj_gen;
        break label_2;
      }
      if (jj_2_1(3)) {
        Method();
      } else {
        switch ((jj_ntk==-1)?jj_ntk():jj_ntk) {
        case BOOLEAN:
        case CHAR:
        case INT:
          Decl();
          break;
        default:
          jj_la1[2] = jj_gen;
          jj_consume_token(-1);
          throw new ParseException();
        }
      }
    }
    jj_consume_token(RBRACE);
  }

  final public void Method() throws ParseException {
    MethodType();
    jj_consume_token(IDENTIFIER);
    ArgumentDecl();
    MethodBody();
  }

  final public void MethodType() throws ParseException {
    switch ((jj_ntk==-1)?jj_ntk():jj_ntk) {
    case BOOLEAN:
    case CHAR:
    case INT:
      Type();
      break;
    case VOID:
      jj_consume_token(VOID);
      break;
    default:
      jj_la1[3] = jj_gen;
      jj_consume_token(-1);
      throw new ParseException();
    }
  }

  final public void Type() throws ParseException {
    switch ((jj_ntk==-1)?jj_ntk():jj_ntk) {
    case INT:
      jj_consume_token(INT);
      break;
    case CHAR:
      jj_consume_token(CHAR);
      break;
    case BOOLEAN:
      jj_consume_token(BOOLEAN);
      break;
    default:
      jj_la1[4] = jj_gen;
      jj_consume_token(-1);
      throw new ParseException();
    }
  }

  final public void ArgumentDecl() throws ParseException {
    jj_consume_token(LPAREN);
    switch ((jj_ntk==-1)?jj_ntk():jj_ntk) {
    case BOOLEAN:
    case CHAR:
    case INT:
      Argument();
      label_3:
      while (true) {
        switch ((jj_ntk==-1)?jj_ntk():jj_ntk) {
        case COMMA:
          ;
          break;
        default:
          jj_la1[5] = jj_gen;
          break label_3;
        }
        jj_consume_token(COMMA);
        Argument();
      }
      break;
    default:
      jj_la1[6] = jj_gen;
      ;
    }
    jj_consume_token(RPAREN);
  }

  final public void Argument() throws ParseException {
    Type();
    jj_consume_token(IDENTIFIER);
  }

  final public void MethodBody() throws ParseException {
    jj_consume_token(LBRACE);
    label_4:
    while (true) {
      switch ((jj_ntk==-1)?jj_ntk():jj_ntk) {
      case BOOLEAN:
      case CHAR:
      case IF:
      case INT:
      case RETURN:
      case WHILE:
      case IDENTIFIER:
      case LBRACE:
      case SEMICOLON:
        ;
        break;
      default:
        jj_la1[7] = jj_gen;
        break label_4;
      }
      Statement();
    }
    jj_consume_token(RBRACE);
  }

/**
 * Reconoce una sentencia del lenguaje
 */
  final public void Statement() throws ParseException {
    switch ((jj_ntk==-1)?jj_ntk():jj_ntk) {
    case BOOLEAN:
    case CHAR:
    case INT:
      Decl();
      break;
    case IDENTIFIER:
      IdStm();
      break;
    case IF:
      IfStm();
      break;
    case WHILE:
      WhileStm();
      break;
    case RETURN:
      ReturnStm();
      break;
    case SEMICOLON:
      NoStm();
      break;
    case LBRACE:
      BlockStm();
      break;
    default:
      jj_la1[8] = jj_gen;
      jj_consume_token(-1);
      throw new ParseException();
    }
  }

/**
 * Reconoce la declaración de una variable (o una lista de variables)
 */
  final public void Decl() throws ParseException {
    Type();
    jj_consume_token(IDENTIFIER);
    Assignement();
    label_5:
    while (true) {
      switch ((jj_ntk==-1)?jj_ntk():jj_ntk) {
      case COMMA:
        ;
        break;
      default:
        jj_la1[9] = jj_gen;
        break label_5;
      }
      jj_consume_token(COMMA);
      jj_consume_token(IDENTIFIER);
      Assignement();
    }
    jj_consume_token(SEMICOLON);
  }

///Bs: Es posible iniciar la variable O No
  final public void Assignement() throws ParseException {
    switch ((jj_ntk==-1)?jj_ntk():jj_ntk) {
    case ASSIGN:
      jj_consume_token(ASSIGN);
      Expr();
      break;
    default:
      jj_la1[10] = jj_gen;
      ;
    }
  }

/**
 * Reconoce una instrucción "if".
 *
 * IfStm -> <IF> <LPAREN> Expr <RPAREN> Stm  ( <ELSE> Stm )?
 */
  final public void IfStm() throws ParseException {
    jj_consume_token(IF);
    jj_consume_token(LPAREN);
    Expr();
    jj_consume_token(RPAREN);
    Statement();
    switch ((jj_ntk==-1)?jj_ntk():jj_ntk) {
    case ELSE:
      jj_consume_token(ELSE);
      Statement();
      break;
    default:
      jj_la1[11] = jj_gen;
      ;
    }
  }

/**
 * Reconoce una instrucción "while".
 *
 * WhileStm -> <WHILE> <LPAREN> Expr <RPAREN> Stm
 */
  final public void WhileStm() throws ParseException {
    jj_consume_token(WHILE);
    jj_consume_token(LPAREN);
    Expr();
    jj_consume_token(RPAREN);
    Statement();
  }

/**
 * Reconoce una instrucción "return".
 *
 * ReturnStm -> <RETURN> ( Expr )? <SEMICOLON>
 */
  final public void ReturnStm() throws ParseException {
    jj_consume_token(RETURN);
    switch ((jj_ntk==-1)?jj_ntk():jj_ntk) {
    case FALSE:
    case TRUE:
    case INTEGER_LITERAL:
    case CHAR_LITERAL:
    case IDENTIFIER:
    case LPAREN:
    case NOT:
    case PLUS:
    case MINUS:
      Expr();
      break;
    default:
      jj_la1[12] = jj_gen;
      ;
    }
    jj_consume_token(SEMICOLON);
  }

/**
 * Reconoce una instrucción vacía.
 *
 * NoStm -> <SEMICOLON>
 */
  final public void NoStm() throws ParseException {
    jj_consume_token(SEMICOLON);
  }

/**
 * Reconoce una instrucción que comienza por "id"
 *
 * IdStm -> <ID> ( Assignement | MethodCall | <DOT>  <ID>  MethodCall )  <SEMICOLON>
 */
  final public void IdStm() throws ParseException {
    jj_consume_token(IDENTIFIER);
    switch ((jj_ntk==-1)?jj_ntk():jj_ntk) {
    case ASSIGN:
      jj_consume_token(ASSIGN);
      Expr();
      break;
    case LPAREN:
      MethodCall();
      break;
    case DOT:
      label_6:
      while (true) {
        jj_consume_token(DOT);
        jj_consume_token(IDENTIFIER);
        switch ((jj_ntk==-1)?jj_ntk():jj_ntk) {
        case LPAREN:
          MethodCall();
          break;
        default:
          jj_la1[13] = jj_gen;
          ;
        }
        switch ((jj_ntk==-1)?jj_ntk():jj_ntk) {
        case DOT:
          ;
          break;
        default:
          jj_la1[14] = jj_gen;
          break label_6;
        }
      }
      break;
    default:
      jj_la1[15] = jj_gen;
      jj_consume_token(-1);
      throw new ParseException();
    }
    jj_consume_token(SEMICOLON);
  }

/**
 * Reconoce un bloque de instrucciones entre llaves.
 *
 * BlockStm -> <LBRACE> ( Stm )* <RBRACE>
 */
  final public void BlockStm() throws ParseException {
    jj_consume_token(LBRACE);
    label_7:
    while (true) {
      switch ((jj_ntk==-1)?jj_ntk():jj_ntk) {
      case BOOLEAN:
      case CHAR:
      case IF:
      case INT:
      case RETURN:
      case WHILE:
      case IDENTIFIER:
      case LBRACE:
      case SEMICOLON:
        ;
        break;
      default:
        jj_la1[16] = jj_gen;
        break label_7;
      }
      Statement();
    }
    jj_consume_token(RBRACE);
  }

/**************************************************************/
/*       EL AN�?LISIS DE LAS EXPRESIONES COMIENZA AQU�?         */
/**************************************************************/
  final public void Expr() throws ParseException {
    AndExpr();
    label_8:
    while (true) {
      switch ((jj_ntk==-1)?jj_ntk():jj_ntk) {
      case OR:
        ;
        break;
      default:
        jj_la1[17] = jj_gen;
        break label_8;
      }
      jj_consume_token(OR);
      AndExpr();
    }
  }

  final public void AndExpr() throws ParseException {
    RelExpr();
    label_9:
    while (true) {
      switch ((jj_ntk==-1)?jj_ntk():jj_ntk) {
      case AND:
        ;
        break;
      default:
        jj_la1[18] = jj_gen;
        break label_9;
      }
      jj_consume_token(AND);
      RelExpr();
    }
  }

  final public void RelExpr() throws ParseException {
    SumExpr();
    switch ((jj_ntk==-1)?jj_ntk():jj_ntk) {
    case EQ:
    case LE:
    case GT:
    case LT:
    case GE:
    case NE:
      RelOp();
      SumExpr();
      break;
    default:
      jj_la1[19] = jj_gen;
      ;
    }
  }

  final public void RelOp() throws ParseException {
    switch ((jj_ntk==-1)?jj_ntk():jj_ntk) {
    case EQ:
      jj_consume_token(EQ);
      break;
    case NE:
      jj_consume_token(NE);
      break;
    case GT:
      jj_consume_token(GT);
      break;
    case GE:
      jj_consume_token(GE);
      break;
    case LT:
      jj_consume_token(LT);
      break;
    case LE:
      jj_consume_token(LE);
      break;
    default:
      jj_la1[20] = jj_gen;
      jj_consume_token(-1);
      throw new ParseException();
    }
  }

  final public void SumExpr() throws ParseException {
    UnOp();
    ProdExpr();
    label_10:
    while (true) {
      switch ((jj_ntk==-1)?jj_ntk():jj_ntk) {
      case PLUS:
      case MINUS:
        ;
        break;
      default:
        jj_la1[21] = jj_gen;
        break label_10;
      }
      SumOp();
      ProdExpr();
    }
  }

  final public void UnOp() throws ParseException {
    switch ((jj_ntk==-1)?jj_ntk():jj_ntk) {
    case NOT:
    case PLUS:
    case MINUS:
      switch ((jj_ntk==-1)?jj_ntk():jj_ntk) {
      case NOT:
        jj_consume_token(NOT);
        break;
      case MINUS:
        jj_consume_token(MINUS);
        break;
      case PLUS:
        jj_consume_token(PLUS);
        break;
      default:
        jj_la1[22] = jj_gen;
        jj_consume_token(-1);
        throw new ParseException();
      }
      break;
    default:
      jj_la1[23] = jj_gen;
      ;
    }
  }

  final public void SumOp() throws ParseException {
    switch ((jj_ntk==-1)?jj_ntk():jj_ntk) {
    case MINUS:
      jj_consume_token(MINUS);
      break;
    case PLUS:
      jj_consume_token(PLUS);
      break;
    default:
      jj_la1[24] = jj_gen;
      jj_consume_token(-1);
      throw new ParseException();
    }
  }

  final public void ProdExpr() throws ParseException {
    Factor();
    label_11:
    while (true) {
      switch ((jj_ntk==-1)?jj_ntk():jj_ntk) {
      case PROD:
      case DIV:
      case MOD:
        ;
        break;
      default:
        jj_la1[25] = jj_gen;
        break label_11;
      }
      MultOp();
      Factor();
    }
  }

  final public void MultOp() throws ParseException {
    switch ((jj_ntk==-1)?jj_ntk():jj_ntk) {
    case PROD:
      jj_consume_token(PROD);
      break;
    case DIV:
      jj_consume_token(DIV);
      break;
    case MOD:
      jj_consume_token(MOD);
      break;
    default:
      jj_la1[26] = jj_gen;
      jj_consume_token(-1);
      throw new ParseException();
    }
  }

  final public void Factor() throws ParseException {
    switch ((jj_ntk==-1)?jj_ntk():jj_ntk) {
    case FALSE:
    case TRUE:
    case INTEGER_LITERAL:
    case CHAR_LITERAL:
      Literal();
      break;
    case IDENTIFIER:
      Reference();
      break;
    case LPAREN:
      jj_consume_token(LPAREN);
      Expr();
      jj_consume_token(RPAREN);
      break;
    default:
      jj_la1[27] = jj_gen;
      jj_consume_token(-1);
      throw new ParseException();
    }
  }

  final public void Literal() throws ParseException {
    switch ((jj_ntk==-1)?jj_ntk():jj_ntk) {
    case INTEGER_LITERAL:
      jj_consume_token(INTEGER_LITERAL);
      break;
    case CHAR_LITERAL:
      jj_consume_token(CHAR_LITERAL);
      break;
    case TRUE:
      jj_consume_token(TRUE);
      break;
    case FALSE:
      jj_consume_token(FALSE);
      break;
    default:
      jj_la1[28] = jj_gen;
      jj_consume_token(-1);
      throw new ParseException();
    }
  }

  final public void Reference() throws ParseException {
    jj_consume_token(IDENTIFIER);
    switch ((jj_ntk==-1)?jj_ntk():jj_ntk) {
    case LPAREN:
    case DOT:
      switch ((jj_ntk==-1)?jj_ntk():jj_ntk) {
      case LPAREN:
        MethodCall();
        break;
      case DOT:
        label_12:
        while (true) {
          jj_consume_token(DOT);
          jj_consume_token(IDENTIFIER);
          switch ((jj_ntk==-1)?jj_ntk():jj_ntk) {
          case LPAREN:
            MethodCall();
            break;
          default:
            jj_la1[29] = jj_gen;
            ;
          }
          switch ((jj_ntk==-1)?jj_ntk():jj_ntk) {
          case DOT:
            ;
            break;
          default:
            jj_la1[30] = jj_gen;
            break label_12;
          }
        }
        break;
      default:
        jj_la1[31] = jj_gen;
        jj_consume_token(-1);
        throw new ParseException();
      }
      break;
    default:
      jj_la1[32] = jj_gen;
      ;
    }
  }

/**
 * Símbolo que reconoce los parámetros de llamada a un método.
 * Por ejemplo: " ( 3, a, b+c ) "
 *
 * MethodCall -> <LPAREN> ( Expr  ( <COMMA> Expr )* )?  <RPAREN>
 */
  final public void MethodCall() throws ParseException {
    jj_consume_token(LPAREN);
    switch ((jj_ntk==-1)?jj_ntk():jj_ntk) {
    case FALSE:
    case TRUE:
    case INTEGER_LITERAL:
    case CHAR_LITERAL:
    case IDENTIFIER:
    case LPAREN:
    case NOT:
    case PLUS:
    case MINUS:
      Expr();
      label_13:
      while (true) {
        switch ((jj_ntk==-1)?jj_ntk():jj_ntk) {
        case COMMA:
          ;
          break;
        default:
          jj_la1[33] = jj_gen;
          break label_13;
        }
        jj_consume_token(COMMA);
        Expr();
      }
      break;
    default:
      jj_la1[34] = jj_gen;
      ;
    }
    jj_consume_token(RPAREN);
  }

  private boolean jj_2_1(int xla) {
    jj_la = xla; jj_lastpos = jj_scanpos = token;
    try { return !jj_3_1(); }
    catch(LookaheadSuccess ls) { return true; }
    finally { jj_save(0, xla); }
  }

  private boolean jj_3R_18() {
    Token xsp;
    xsp = jj_scanpos;
    if (jj_scan_token(14)) {
    jj_scanpos = xsp;
    if (jj_scan_token(9)) {
    jj_scanpos = xsp;
    if (jj_scan_token(8)) return true;
    }
    }
    return false;
  }

  private boolean jj_3R_16() {
    if (jj_scan_token(LPAREN)) return true;
    return false;
  }

  private boolean jj_3R_14() {
    if (jj_3R_15()) return true;
    if (jj_scan_token(IDENTIFIER)) return true;
    if (jj_3R_16()) return true;
    return false;
  }

  private boolean jj_3R_17() {
    if (jj_3R_18()) return true;
    return false;
  }

  private boolean jj_3R_15() {
    Token xsp;
    xsp = jj_scanpos;
    if (jj_3R_17()) {
    jj_scanpos = xsp;
    if (jj_scan_token(18)) return true;
    }
    return false;
  }

  private boolean jj_3_1() {
    if (jj_3R_14()) return true;
    return false;
  }

  /** Generated Token Manager. */
  public TintoParserV1TokenManager token_source;
  SimpleCharStream jj_input_stream;
  /** Current token. */
  public Token token;
  /** Next token. */
  public Token jj_nt;
  private int jj_ntk;
  private Token jj_scanpos, jj_lastpos;
  private int jj_la;
  private int jj_gen;
  final private int[] jj_la1 = new int[35];
  static private int[] jj_la1_0;
  static private int[] jj_la1_1;
  static {
      jj_la1_init_0();
      jj_la1_init_1();
   }
   private static void jj_la1_init_0() {
      jj_la1_0 = new int[] {0x2000,0x44300,0x4300,0x44300,0x4300,0x80000000,0x4300,0x52095300,0x52095300,0x80000000,0x0,0x400,0x7120800,0x4000000,0x0,0x4000000,0x52095300,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x7120800,0x1120800,0x4000000,0x0,0x4000000,0x4000000,0x80000000,0x7120800,};
   }
   private static void jj_la1_init_1() {
      jj_la1_1 = new int[] {0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x2,0x0,0x1c00,0x0,0x1,0x3,0x0,0x100,0x200,0xfc,0xfc,0x1800,0x1c00,0x1c00,0x1800,0xe000,0xe000,0x0,0x0,0x0,0x1,0x1,0x1,0x0,0x1c00,};
   }
  final private JJCalls[] jj_2_rtns = new JJCalls[1];
  private boolean jj_rescan = false;
  private int jj_gc = 0;

  /** Constructor with InputStream. */
  public TintoParserV1(java.io.InputStream stream) {
     this(stream, null);
  }
  /** Constructor with InputStream and supplied encoding */
  public TintoParserV1(java.io.InputStream stream, String encoding) {
    try { jj_input_stream = new SimpleCharStream(stream, encoding, 1, 1); } catch(java.io.UnsupportedEncodingException e) { throw new RuntimeException(e); }
    token_source = new TintoParserV1TokenManager(jj_input_stream);
    token = new Token();
    jj_ntk = -1;
    jj_gen = 0;
    for (int i = 0; i < 35; i++) jj_la1[i] = -1;
    for (int i = 0; i < jj_2_rtns.length; i++) jj_2_rtns[i] = new JJCalls();
  }

  /** Reinitialise. */
  public void ReInit(java.io.InputStream stream) {
     ReInit(stream, null);
  }
  /** Reinitialise. */
  public void ReInit(java.io.InputStream stream, String encoding) {
    try { jj_input_stream.ReInit(stream, encoding, 1, 1); } catch(java.io.UnsupportedEncodingException e) { throw new RuntimeException(e); }
    token_source.ReInit(jj_input_stream);
    token = new Token();
    jj_ntk = -1;
    jj_gen = 0;
    for (int i = 0; i < 35; i++) jj_la1[i] = -1;
    for (int i = 0; i < jj_2_rtns.length; i++) jj_2_rtns[i] = new JJCalls();
  }

  /** Constructor. */
  public TintoParserV1(java.io.Reader stream) {
    jj_input_stream = new SimpleCharStream(stream, 1, 1);
    token_source = new TintoParserV1TokenManager(jj_input_stream);
    token = new Token();
    jj_ntk = -1;
    jj_gen = 0;
    for (int i = 0; i < 35; i++) jj_la1[i] = -1;
    for (int i = 0; i < jj_2_rtns.length; i++) jj_2_rtns[i] = new JJCalls();
  }

  /** Reinitialise. */
  public void ReInit(java.io.Reader stream) {
    jj_input_stream.ReInit(stream, 1, 1);
    token_source.ReInit(jj_input_stream);
    token = new Token();
    jj_ntk = -1;
    jj_gen = 0;
    for (int i = 0; i < 35; i++) jj_la1[i] = -1;
    for (int i = 0; i < jj_2_rtns.length; i++) jj_2_rtns[i] = new JJCalls();
  }

  /** Constructor with generated Token Manager. */
  public TintoParserV1(TintoParserV1TokenManager tm) {
    token_source = tm;
    token = new Token();
    jj_ntk = -1;
    jj_gen = 0;
    for (int i = 0; i < 35; i++) jj_la1[i] = -1;
    for (int i = 0; i < jj_2_rtns.length; i++) jj_2_rtns[i] = new JJCalls();
  }

  /** Reinitialise. */
  public void ReInit(TintoParserV1TokenManager tm) {
    token_source = tm;
    token = new Token();
    jj_ntk = -1;
    jj_gen = 0;
    for (int i = 0; i < 35; i++) jj_la1[i] = -1;
    for (int i = 0; i < jj_2_rtns.length; i++) jj_2_rtns[i] = new JJCalls();
  }

  private Token jj_consume_token(int kind) throws ParseException {
    Token oldToken;
    if ((oldToken = token).next != null) token = token.next;
    else token = token.next = token_source.getNextToken();
    jj_ntk = -1;
    if (token.kind == kind) {
      jj_gen++;
      if (++jj_gc > 100) {
        jj_gc = 0;
        for (int i = 0; i < jj_2_rtns.length; i++) {
          JJCalls c = jj_2_rtns[i];
          while (c != null) {
            if (c.gen < jj_gen) c.first = null;
            c = c.next;
          }
        }
      }
      return token;
    }
    token = oldToken;
    jj_kind = kind;
    throw generateParseException();
  }

  static private final class LookaheadSuccess extends java.lang.Error { }
  final private LookaheadSuccess jj_ls = new LookaheadSuccess();
  private boolean jj_scan_token(int kind) {
    if (jj_scanpos == jj_lastpos) {
      jj_la--;
      if (jj_scanpos.next == null) {
        jj_lastpos = jj_scanpos = jj_scanpos.next = token_source.getNextToken();
      } else {
        jj_lastpos = jj_scanpos = jj_scanpos.next;
      }
    } else {
      jj_scanpos = jj_scanpos.next;
    }
    if (jj_rescan) {
      int i = 0; Token tok = token;
      while (tok != null && tok != jj_scanpos) { i++; tok = tok.next; }
      if (tok != null) jj_add_error_token(kind, i);
    }
    if (jj_scanpos.kind != kind) return true;
    if (jj_la == 0 && jj_scanpos == jj_lastpos) throw jj_ls;
    return false;
  }


/** Get the next Token. */
  final public Token getNextToken() {
    if (token.next != null) token = token.next;
    else token = token.next = token_source.getNextToken();
    jj_ntk = -1;
    jj_gen++;
    return token;
  }

/** Get the specific Token. */
  final public Token getToken(int index) {
    Token t = token;
    for (int i = 0; i < index; i++) {
      if (t.next != null) t = t.next;
      else t = t.next = token_source.getNextToken();
    }
    return t;
  }

  private int jj_ntk() {
    if ((jj_nt=token.next) == null)
      return (jj_ntk = (token.next=token_source.getNextToken()).kind);
    else
      return (jj_ntk = jj_nt.kind);
  }

  private java.util.List<int[]> jj_expentries = new java.util.ArrayList<int[]>();
  private int[] jj_expentry;
  private int jj_kind = -1;
  private int[] jj_lasttokens = new int[100];
  private int jj_endpos;

  private void jj_add_error_token(int kind, int pos) {
    if (pos >= 100) return;
    if (pos == jj_endpos + 1) {
      jj_lasttokens[jj_endpos++] = kind;
    } else if (jj_endpos != 0) {
      jj_expentry = new int[jj_endpos];
      for (int i = 0; i < jj_endpos; i++) {
        jj_expentry[i] = jj_lasttokens[i];
      }
      jj_entries_loop: for (java.util.Iterator<?> it = jj_expentries.iterator(); it.hasNext();) {
        int[] oldentry = (int[])(it.next());
        if (oldentry.length == jj_expentry.length) {
          for (int i = 0; i < jj_expentry.length; i++) {
            if (oldentry[i] != jj_expentry[i]) {
              continue jj_entries_loop;
            }
          }
          jj_expentries.add(jj_expentry);
          break jj_entries_loop;
        }
      }
      if (pos != 0) jj_lasttokens[(jj_endpos = pos) - 1] = kind;
    }
  }

  /** Generate ParseException. */
  public ParseException generateParseException() {
    jj_expentries.clear();
    boolean[] la1tokens = new boolean[48];
    if (jj_kind >= 0) {
      la1tokens[jj_kind] = true;
      jj_kind = -1;
    }
    for (int i = 0; i < 35; i++) {
      if (jj_la1[i] == jj_gen) {
        for (int j = 0; j < 32; j++) {
          if ((jj_la1_0[i] & (1<<j)) != 0) {
            la1tokens[j] = true;
          }
          if ((jj_la1_1[i] & (1<<j)) != 0) {
            la1tokens[32+j] = true;
          }
        }
      }
    }
    for (int i = 0; i < 48; i++) {
      if (la1tokens[i]) {
        jj_expentry = new int[1];
        jj_expentry[0] = i;
        jj_expentries.add(jj_expentry);
      }
    }
    jj_endpos = 0;
    jj_rescan_token();
    jj_add_error_token(0, 0);
    int[][] exptokseq = new int[jj_expentries.size()][];
    for (int i = 0; i < jj_expentries.size(); i++) {
      exptokseq[i] = jj_expentries.get(i);
    }
    return new ParseException(token, exptokseq, tokenImage);
  }

  /** Enable tracing. */
  final public void enable_tracing() {
  }

  /** Disable tracing. */
  final public void disable_tracing() {
  }

  private void jj_rescan_token() {
    jj_rescan = true;
    for (int i = 0; i < 1; i++) {
    try {
      JJCalls p = jj_2_rtns[i];
      do {
        if (p.gen > jj_gen) {
          jj_la = p.arg; jj_lastpos = jj_scanpos = p.first;
          switch (i) {
            case 0: jj_3_1(); break;
          }
        }
        p = p.next;
      } while (p != null);
      } catch(LookaheadSuccess ls) { }
    }
    jj_rescan = false;
  }

  private void jj_save(int index, int xla) {
    JJCalls p = jj_2_rtns[index];
    while (p.gen > jj_gen) {
      if (p.next == null) { p = p.next = new JJCalls(); break; }
      p = p.next;
    }
    p.gen = jj_gen + xla - jj_la; p.first = token; p.arg = xla;
  }

  static final class JJCalls {
    int gen;
    Token first;
    int arg;
    JJCalls next;
  }

}
