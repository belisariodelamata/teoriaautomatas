options {
  CHOICE_AMBIGUITY_CHECK = 2;
  OTHER_AMBIGUITY_CHECK = 1;
  STATIC = false;
  DEBUG_PARSER = false;
  DEBUG_LOOKAHEAD = false;
  DEBUG_TOKEN_MANAGER = false;
  ERROR_REPORTING = true;
  JAVA_UNICODE_ESCAPE = false;
  UNICODE_INPUT = true;
  IGNORE_CASE = false;
  USER_TOKEN_MANAGER = false;
  USER_CHAR_STREAM = false;
  BUILD_PARSER = true;
  BUILD_TOKEN_MANAGER = true;
  SANITY_CHECK = true;
  FORCE_LA_CHECK = false;
  COMMON_TOKEN_ACTION=true;
}
PARSER_BEGIN(TintoParserV1)
//------------------------------------------------------------------//
//                        COPYRIGHT NOTICE                          //
//------------------------------------------------------------------//
// Copyright (c) 2008, Francisco José Moreno Velo                   //
// All rights reserved.                                             //
//                                                                  //
// Redistribution and use in source and binary forms, with or       //
// without modification, are permitted provided that the following  //
// conditions are met:                                              //
//                                                                  //
// * Redistributions of source code must retain the above copyright //
//   notice, this list of conditions and the following disclaimer.  // 
//                                                                  //
// * Redistributions in binary form must reproduce the above        // 
//   copyright notice, this list of conditions and the following    // 
//   disclaimer in the documentation and/or other materials         // 
//   provided with the distribution.                                //
//                                                                  //
// * Neither the name of the University of Huelva nor the names of  //
//   its contributors may be used to endorse or promote products    //
//   derived from this software without specific prior written      // 
//   permission.                                                    //
//                                                                  //
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND           // 
// CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,      // 
// INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF         // 
// MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE         // 
// DISCLAIMED. IN NO EVENT SHALL THE COPRIGHT OWNER OR CONTRIBUTORS //
// BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,         // 
// EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED  //
// TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,    //
// DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND   // 
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT          //
// LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING   //
// IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF   //
// THE POSSIBILITY OF SUCH DAMAGE.                                  //
//------------------------------------------------------------------//

//------------------------------------------------------------------//
//                      Universidad de Huelva                       //
//          Departamento de Tecnologías de la Información           //
//   Área de Ciencias de la Computación e Inteligencia Artificial   //
//------------------------------------------------------------------//
//                     PROCESADORES DE LENGUAJE                     //
//------------------------------------------------------------------//
//                                                                  //
//          Compilador del lenguaje Tinto [Versión 0.1]             //
//                                                                  //
//------------------------------------------------------------------//


package tinto;

public class TintoParserV1 implements co.InterfaceAnalizador{
    public void inicio() throws Exception{
        CompilationUnit();
    }
    public String textoLeido(){
        return "";
    }
}


PARSER_END(TintoParserV1)
TOKEN_MGR_DECLS : {
    java.util.List<Token> listaToken=new java.util.LinkedList<Token>();
    void CommonTokenAction(Token t){
        listaToken.add(t);
        System.out.println(t);
    }
    public java.util.List<Token> getListaToken(){
        return listaToken;
    }
}

/* WHITE SPACE */

SKIP :
{
  " "
| "\t"
| "\n"
| "\r"
| "\f"
}

/* COMMENTS */

SKIP :
{
  <SINGLE_LINE_COMMENT: "//" ( ~["\n","\r"] )* ("\n" | "\r" | "\r\n") >
|
  <MULTI_LINE_COMMENT: "/*" ( ~["*"] | ("*")+ ~["*","/"] )* ("*")+ "/" >
}


/* RESERVED WORDS */

TOKEN :
{
  < BOOLEAN: "boolean" >
| < CHAR: "char" >
| < ELSE: "else" >
| < FALSE: "false" >
| < IF: "if" >
| < IMPORT: "import" >
| < INT: "int" >
| < LIBRARY: "library" >
| < RETURN: "return" >
| < TRUE: "true" >
| < VOID: "void" >
| < WHILE: "while" >
}

/* LITERALS */

TOKEN :
{
  < INTEGER_LITERAL: ( <DECIMAL_LITERAL> | <HEX_LITERAL> | <OCTAL_LITERAL> ) >
|
  < #DECIMAL_LITERAL: ["1"-"9"] (["0"-"9"])* >
|
  < #HEX_LITERAL: "0" ["x","X"] (["0"-"9","a"-"f","A"-"F"])+ >
|
  < #OCTAL_LITERAL: "0" (["0"-"7"])* >
|
  < CHAR_LITERAL:
      "'"
      (   (~["'","\\","\n","\r"])
        | ("\\"
            ( ["n","t","b","r","f","\\","'","\""]
            | ["0"-"7"] ( ["0"-"7"] )?
            | ["0"-"3"] ["0"-"7"] ["0"-"7"]
            )
          )
      )
      "'"
  >
}

/* IDENTIFIERS */

TOKEN :
{
  < IDENTIFIER: ["a"-"z","A"-"Z","_"] ( ["a"-"z","A"-"Z","0"-"9","_"] )* >
}

/* SEPARATORS */

TOKEN :
{
  < LPAREN: "(" >
| < RPAREN: ")" >
| < LBRACE: "{" >
| < RBRACE: "}" >
| < SEMICOLON: ";" >
| < COMMA: "," >
| < DOT: "." >
}

/* OPERATORS */

TOKEN :
{
  < ASSIGN: "=" >
| < EQ: "==" >
| < LE: "<=" >
| < GT: ">" >
| < LT: "<" >
| < GE: ">=" >
| < NE: "!=" >
| < OR: "||" >
| < AND: "&&" >
| < NOT: "!" >
| < PLUS: "+" >
| < MINUS: "-" >
| < PROD: "*" >
| < DIV: "/" >
| < MOD: "%" >
}


/**************************************************************/
/*         EL ANÁLISIS DE LA CABECERA COMIENZA AQUÍ           */
/**************************************************************/

/**
 * Reconoce el contenido completo de un archivo ".tinto"
 *
 * CompilationUnit -> (InportClause)* Library
 */
void CompilationUnit():
{}
{ 
  ( ImportClause() )*  Library()
}

void ImportClause():
{}
{
  <IMPORT>  <IDENTIFIER>  <SEMICOLON> 
}

void Library() :
{}
{
  <LIBRARY>  <IDENTIFIER> <LBRACE> (LOOKAHEAD(3) Method()|Decl())* <RBRACE>
}

void Method() :
{}
{
  MethodType() <IDENTIFIER>  ArgumentDecl() MethodBody()
}

void MethodType() :
{}
{
   Type()
 | <VOID>
}

void Type() :
{}
{
   <INT>      
 | <CHAR>     
 | <BOOLEAN>  
}

void ArgumentDecl() :
{ }
{
//    <LPAREN>  (  Argument() ( <COMMA>  Argument() )* )?  <RPAREN>  
    <LPAREN>  [  Argument() ( <COMMA>  Argument() )* ]  <RPAREN>  
}

void Argument() :
{ }
{
    Type()  <IDENTIFIER>
}

void MethodBody() :
{ }
{
   <LBRACE>  (  Statement() )*  <RBRACE>
}

/**
 * Reconoce una sentencia del lenguaje
 */
void Statement() :
{ }
{
 (
   Decl()
 | IdStm()
 | IfStm()
 | WhileStm()
 | ReturnStm()
 | NoStm()
 | BlockStm()
 )
}

/**
 * Reconoce la declaración de una variable (o una lista de variables)
 */
void Decl() :
{ }
{
   Type() <IDENTIFIER>  Assignement()  ( <COMMA> <IDENTIFIER> Assignement() )*  <SEMICOLON>
}

///Bs: Es posible iniciar la variable O No
void Assignement() :
{ }
{
  ( <ASSIGN>  Expr() )?  
}

/**
 * Reconoce una instrucción "if".
 *
 * IfStm -> <IF> <LPAREN> Expr <RPAREN> Stm  ( <ELSE> Stm )?
 */
void IfStm() :
{ }
{
  <IF>  <LPAREN>  Expr()  <RPAREN>  Statement()  
  ( LOOKAHEAD( 1 ) <ELSE>  Statement() )?
//  ( <ELSE>  Statement() )?
}

/**
 * Reconoce una instrucción "while".
 *
 * WhileStm -> <WHILE> <LPAREN> Expr <RPAREN> Stm
 */
void WhileStm() :
{ }
{
  <WHILE>  <LPAREN>  Expr()  <RPAREN>  Statement()
}

/**
 * Reconoce una instrucción "return".
 *
 * ReturnStm -> <RETURN> ( Expr )? <SEMICOLON>
 */
void ReturnStm() :
{ }
{
  <RETURN>  ( Expr() )?  <SEMICOLON>
}

/**
 * Reconoce una instrucción vacía.
 *
 * NoStm -> <SEMICOLON>
 */
void NoStm() :
{}
{
  <SEMICOLON>
}

/**
 * Reconoce una instrucción que comienza por "id"
 *
 * IdStm -> <ID> ( Assignement | MethodCall | <DOT>  <ID>  MethodCall )  <SEMICOLON>
 */
void  IdStm() :
{ }
{
//  <IDENTIFIER>  ( <ASSIGN> Expr() | MethodCall() | <DOT>  <IDENTIFIER>  MethodCall() ) <SEMICOLON> 
  <IDENTIFIER>  ( <ASSIGN> Expr() | MethodCall() | (<DOT>  <IDENTIFIER>  (MethodCall())?)+ ) <SEMICOLON> 
}

/**
 * Reconoce un bloque de instrucciones entre llaves.
 *
 * BlockStm -> <LBRACE> ( Stm )* <RBRACE>
 */
void BlockStm() :
{ }
{
  <LBRACE> ( Statement() )* <RBRACE>  
}

/**************************************************************/
/*       EL ANÁLISIS DE LAS EXPRESIONES COMIENZA AQUÍ         */
/**************************************************************/

void Expr() :
{ }
{
  AndExpr() ( <OR>  AndExpr()  )*
}

void AndExpr() :
{ }
{
  RelExpr() ( <AND> RelExpr()  )*
}

void RelExpr() :
{ }
{
   SumExpr() ( RelOp() SumExpr() )?
}

void RelOp() :
{}
{
   <EQ> 
 | <NE> 
 | <GT> 
 | <GE> 
 | <LT> 
 | <LE> 
}

void SumExpr() :
{ }
{
	UnOp() ProdExpr() ( SumOp() ProdExpr() )*
}

void UnOp() :
{ }
{
  ( <NOT> | <MINUS>  | <PLUS>  )?
}

void SumOp() :
{ }
{
  ( <MINUS>  | <PLUS>  )
}	

void ProdExpr() :
{ }
{
	Factor() ( MultOp() Factor() )*  
}

void MultOp() :
{ }
{
  ( <PROD>  | <DIV> | <MOD> )
}	

void Factor() :
{ }
{
	( Literal() | Reference() |	<LPAREN>  Expr()  <RPAREN> )
}

void Literal() :
{ }
{
   <INTEGER_LITERAL>    
 | <CHAR_LITERAL>       
 | <TRUE>               
 | <FALSE>              
}

void Reference() :
{ }
{
//  <IDENTIFIER>  ( MethodCall() | <DOT>  <IDENTIFIER>  MethodCall() )? 
  <IDENTIFIER>  ( MethodCall() | (<DOT>  <IDENTIFIER>  (MethodCall())?)+ )? 
}

/**
 * Símbolo que reconoce los parámetros de llamada a un método.
 * Por ejemplo: " ( 3, a, b+c ) "
 *
 * MethodCall -> <LPAREN> ( Expr  ( <COMMA> Expr )* )?  <RPAREN>
 */
void MethodCall() :
{ }
{
   <LPAREN> ( Expr()  ( <COMMA> Expr() )* )?  <RPAREN>
}