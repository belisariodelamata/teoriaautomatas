/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co;

/**
 *
 * @author Ing Belisario
 */
public interface InterfaceAnalizador {
    public void inicio() throws Exception;
    public String textoLeido();
}
