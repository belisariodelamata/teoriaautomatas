PARSER_BEGIN(Calculadora)
    package co.calculadoraConTipo;
    import java.util.*;
    public class Calculadora{
        static TablaSimbolos tabla = new TablaSimbolos();
        public static void main(String args[]) throws ParseException {
            new Calculadora(System.in).gramatica();
            tabla.imprimir();
        }
        private static char tipo(Object o){
            if (o == null) return 'i';
            else if (o instanceof Integer) return 'e';
            else return 'c';
         }
        private static void usarIMPRIMIR(Object e){
            if (tipo(e) == 'i')
            System.out.println("Indefinido.");
            else
            System.out.println(e.toString());
        }
        private static void usarASIG(Simbolo s, Object e){
            if (tipo(e) == 'i')
            System.out.println("Asignacion no efectuada.");
            else
            if ((s.valor == null) || (tipo(s.valor) == tipo(e)))
            s.valor = e;
            else
            System.err.println("Asignacion de tipos incompatibles.");
        }
        private static Object usarMAS(Object e1, Object e2){
            if((tipo(e1) == 'c') && (tipo(e2) == 'c'))
                return e1.toString() + e2.toString();
            else if((tipo(e1) == 'e') && (tipo(e2) == 'e'))
                return new Integer(((Integer)e1).intValue() + ((Integer)e2).intValue());
            else {
                if ((tipo(e1) != 'i') && (tipo(e2) != 'i'))
                System.err.println("No se pueden sumar cadenas y enteros.");
                return null;
            }
        }
        private static Object usarPOR(Object e1, Object e2){
            if((tipo(e1) == 'c') || (tipo(e2) == 'c')) {
            System.err.println("Una cadena no se puede multiplicar.");
            return null;
            } else if((tipo(e1) == 'e') && (tipo(e2) == 'e'))
            return new Integer(((Integer)e1).intValue() * ((Integer)e2).intValue());
            else
            return null;
        }
        private static Object usarID(Simbolo s){
            if (tipo(s.valor) == 'i')
            System.err.println("Tipo de "+ s.nombre +" no definido.");
            return s.valor;
        }
        private static Object usarA_CADENA(Object e){
            if (tipo(e) != 'e') {
                System.err.println("Error de conversión. Se requiere un entero.");
                return null;
            } else
                return e.toString();
        }
        private static Object usarA_ENTERO(Object e){
            if (tipo(e) != 'c') {
                System.err.println("Error de conversión. Se requiere una cadena.");
                return null;
            } else try {
                return new Integer(Integer.parseInt(e.toString()));
            } catch (Exception x){
                System.err.println("La cadena a convertir sólo puede tener dígitos.");
                return null;
            }
        }
 }
 PARSER_END(Calculadora)
 SKIP : {
 "b¯&"
 | "\t"
 | "\r"
 }
 TOKEN [IGNORE_CASE] :
 {
 <IMPRIMIR: "PRINT">
 | <A_CADENA: "A_CADENA">
 | <A_ENTERO: "A_ENTERO">
 | <ID: ["A"-"Z"](["A"-"Z", "0"-"9"])*>
 | <NUMERO: (["0"-"9"])+>
 | <CADENA: "\""(~["\""])*"\"">
 | <RETORNODECARRO: "\n">
 }
 /*
 gramatica ::= ( sentFinalizada )*
 */
 void gramatica():{}{
 (sentFinalizada())*
 }
 /*
 sentFinalizada ::= IMPRIMIR expr '\n' | ID ASIG expr '\n' | error '\n'
 */
 void sentFinalizada():{
    Simbolo s;
    Object e;
 }{ try {
        <IMPRIMIR> e=expr() <RETORNODECARRO> { usarIMPRIMIR(e); }
        | s=id() ":=" e=expr() <RETORNODECARRO> { usarASIG(s, e); }
    }catch(ParseException x){
        System.out.println(x.toString());
        Token t;
        do {
            t = getNextToken();
        } while (t.kind != RETORNODECARRO);
    }
 }
 /*
 expr ::= term ('+' term)*
 */
 Object expr():{
 Object t1, t2;
 }{
 t1=term() ( "+" t2=term() { t1=usarMAS(t1, t2); } )* { return t1; }
 }
 /*
 term ::= fact ('*' fact)*
 */
 Object term():{
 Object f1, f2;
 }{
 f1=fact() ( "*" f2=fact() { f1=usarPOR(f1, f2); } )* { return f1; }
 }
 /*
 fact ::= ID | NUMERO | CADENA | A_CADENA '(' expr ')' | A_ENTERO '(' expr ')'
 */
 Object fact():{
    Simbolo s;
    int i;
    String c;
    Object e;
 }{
    s=id() { return usarID(s); }
    | i=numero() { return new Integer(i); }
    | c=cadena() { return c; }
    | <A_CADENA> "(" e=expr() ")" { return usarA_CADENA(e); }
    | <A_ENTERO> "(" e=expr() ")" { return usarA_ENTERO(e); }
 }
 Simbolo id():{}{
    <ID> {
        Simbolo s;
        if ((s = tabla.buscar(token.image)) == null)
        s = tabla.insertar(token.image);
        return s;
    }
 }
 int numero():{}{
     <NUMERO> { return Integer.parseInt(token.image); }
 }
 String cadena():{}{
     <CADENA> { return token.image.substring(1, token.image.length() - 1); }
 }
 SKIP : {
     <ILEGAL: (~[])> { System.out.println("Carácter: "+image+" no esperado.");}
 }
