package interfaz;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import co.InterfaceAnalizador;
import co.traductor.Prueba;
import co.traductor.Token;
import java.util.List;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author BS
 */
public class FrmJavaCCPrueba extends javax.swing.JFrame {

    /**
     * Creates new form FrmMain
     */
    public FrmJavaCCPrueba() {
        initComponents();
        btnAnalizarActionPerformed(null);
    }

    /**
     * **********************************************
     */
    public FileInputStream archivo_guardar(String nomArchivo) throws IOException {
//Un texto cualquiera guardado en una variable
        try {
//Crear un objeto File se encarga de crear o abrir acceso a un archivo que se especifica en su constructor
            File archivo = new File(nomArchivo);
//Crear objeto FileWriter que sera el que nos ayude a escribir sobre archivo
            FileWriter escribir = new FileWriter(archivo, false);
//Escribimos en el archivo con el metodo write 
            escribir.write(txtFunction.getText());
//Cerramos la conexion
            escribir.close();
            return new FileInputStream(nomArchivo);
        } //Si existe un problema al escribir cae aqui
        catch (Exception e) {
            System.out.println("Error al escribir");
        }
        return null;
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        txtFunction = new javax.swing.JTextArea();
        btnAnalizar = new javax.swing.JButton();
        jScrollPane2 = new javax.swing.JScrollPane();
        txtEvaluacion = new javax.swing.JTextArea();
        lblestado = new javax.swing.JLabel();
        lblExpresion = new javax.swing.JLabel();
        lblExpresion1 = new javax.swing.JLabel();
        jScrollPane3 = new javax.swing.JScrollPane();
        txtTraductor = new javax.swing.JTextArea();
        jScrollPane4 = new javax.swing.JScrollPane();
        txtTokens = new javax.swing.JTextArea();
        jScrollPane5 = new javax.swing.JScrollPane();
        txtEstadosAlcanzados = new javax.swing.JTextArea();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Evaluador Sintáctico");

        txtFunction.setColumns(20);
        txtFunction.setRows(5);
        txtFunction.setText("public class hola{\n\tint hola=25,hola=24,tusabes=true;\n}");
        txtFunction.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtFunctionKeyReleased(evt);
            }
        });
        jScrollPane1.setViewportView(txtFunction);

        btnAnalizar.setText("Comprobar Sintáxis");
        btnAnalizar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAnalizarActionPerformed(evt);
            }
        });

        txtEvaluacion.setColumns(20);
        txtEvaluacion.setForeground(new java.awt.Color(255, 0, 0));
        txtEvaluacion.setRows(5);
        txtEvaluacion.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtEvaluacionKeyReleased(evt);
            }
        });
        jScrollPane2.setViewportView(txtEvaluacion);

        lblestado.setBackground(new java.awt.Color(0, 255, 51));
        lblestado.setFont(new java.awt.Font("Tahoma", 3, 11)); // NOI18N
        lblestado.setForeground(new java.awt.Color(255, 255, 255));
        lblestado.setOpaque(true);
        lblestado.setVisible(false);

        lblExpresion.setFont(new java.awt.Font("Tahoma", 3, 11)); // NOI18N
        lblExpresion.setText("Expresión");

        lblExpresion1.setFont(new java.awt.Font("Tahoma", 3, 11)); // NOI18N
        lblExpresion1.setForeground(new java.awt.Color(255, 0, 0));
        lblExpresion1.setText("Errores");

        txtTraductor.setColumns(20);
        txtTraductor.setForeground(new java.awt.Color(255, 0, 0));
        txtTraductor.setRows(5);
        txtTraductor.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtTraductorKeyReleased(evt);
            }
        });
        jScrollPane3.setViewportView(txtTraductor);

        txtTokens.setColumns(20);
        txtTokens.setRows(5);
        jScrollPane4.setViewportView(txtTokens);

        txtEstadosAlcanzados.setColumns(20);
        txtEstadosAlcanzados.setLineWrap(true);
        txtEstadosAlcanzados.setRows(5);
        jScrollPane5.setViewportView(txtEstadosAlcanzados);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(lblExpresion, javax.swing.GroupLayout.PREFERRED_SIZE, 68, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(btnAnalizar, javax.swing.GroupLayout.PREFERRED_SIZE, 177, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(0, 903, Short.MAX_VALUE))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                .addGroup(layout.createSequentialGroup()
                                    .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 270, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                    .addComponent(jScrollPane3, javax.swing.GroupLayout.DEFAULT_SIZE, 251, Short.MAX_VALUE))
                                .addComponent(lblestado, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(jScrollPane1, javax.swing.GroupLayout.Alignment.LEADING))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(lblExpresion1, javax.swing.GroupLayout.PREFERRED_SIZE, 68, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 0, Short.MAX_VALUE)))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jScrollPane5, javax.swing.GroupLayout.PREFERRED_SIZE, 229, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jScrollPane4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(138, 138, 138))))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lblExpresion)
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 166, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 16, Short.MAX_VALUE)
                        .addComponent(lblestado, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(lblExpresion1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 148, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 148, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addComponent(jScrollPane4)
                    .addComponent(jScrollPane5))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnAnalizar))
        );

        setBounds(0, 0, 903, 487);
    }// </editor-fold>//GEN-END:initComponents


    private void btnAnalizarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAnalizarActionPerformed
        txtEvaluacion.setText("");
        FileInputStream tmp;
        //Analizador analizador = null;
        Prueba vacile = null;
        InterfaceAnalizador interf=null;
        try {
            tmp = archivo_guardar("automatas.tmp");

            vacile = new Prueba(tmp);
            System.out.println("------------");
            //Vacile.ReInit(tmp);
            interf = (InterfaceAnalizador) vacile;
            interf.inicio();
            mostrarestado(1, "Expresion evaluada exitosamente");
            return;
        } catch (Exception ex) {
            txtEvaluacion.setText(ex.getMessage());
            //Logger.getLogger(FrmMain_1.class.getName()).log(Level.SEVERE, null, ex);
        } finally{
            listarTokens(((Prueba)vacile).token_source.getListaToken());
            txtEstadosAlcanzados.setText(vacile.getEstadosAlcanzados());
        }
        mostrarestado(2, "Expresion con sintáxis errónea");
    }//GEN-LAST:event_btnAnalizarActionPerformed
    private void listarTokens(List<Token> tokens){
        StringBuilder sb=new StringBuilder();
        for(Token t: tokens){
            sb.append(t+("->"+(t.next))+"->["+t.beginLine+","+t.beginColumn+"]"+"["+t.endLine+","+t.endColumn+"]\r\n");
        }
        txtTokens.setText(sb.toString());
    }
    private void txtEvaluacionKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtEvaluacionKeyReleased
        // TODO add your handling code here:
    }//GEN-LAST:event_txtEvaluacionKeyReleased

    private void txtFunctionKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtFunctionKeyReleased
        btnAnalizarActionPerformed(null);
        // TODO add your handling code here:
    }//GEN-LAST:event_txtFunctionKeyReleased

    private void txtTraductorKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtTraductorKeyReleased
        // TODO add your handling code here:
    }//GEN-LAST:event_txtTraductorKeyReleased

    //animacion de estado//////////
    javax.swing.Timer timerestado = new javax.swing.Timer(3000, new ActionListener() {

        public void actionPerformed(ActionEvent e) {
            lblestado.setVisible(false);
        }
    });
    ///////////////////////////////

    private void mostrarestado(int estado, String msg) {
        //dedicado a Jonathan
        //el monstruo en interfaz...
        timerestado.stop();
        if (estado == 1) {
            lblestado.setBackground(Color.blue);
            lblestado.setIcon(new javax.swing.ImageIcon(getClass().getResource("/interfaz/1.gif")));
        } else {
            lblestado.setBackground(Color.red);
            lblestado.setIcon(new javax.swing.ImageIcon(getClass().getResource("/interfaz/2.gif")));
        }
        lblestado.setText(msg);
        lblestado.setVisible(true);
        timerestado.start();
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /*
         * Set the Nimbus look and feel
         */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /*
         * If Nimbus (introduced in Java SE 6) is not available, stay with the
         * default look and feel. For details see
         * http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(FrmJavaCCPrueba.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(FrmJavaCCPrueba.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(FrmJavaCCPrueba.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(FrmJavaCCPrueba.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>

        /*
         * Create and display the form
         */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new FrmJavaCCPrueba().setVisible(true);
            }
        });
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAnalizar;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JScrollPane jScrollPane4;
    private javax.swing.JScrollPane jScrollPane5;
    private javax.swing.JLabel lblExpresion;
    private javax.swing.JLabel lblExpresion1;
    private javax.swing.JLabel lblestado;
    private javax.swing.JTextArea txtEstadosAlcanzados;
    private javax.swing.JTextArea txtEvaluacion;
    private javax.swing.JTextArea txtFunction;
    private javax.swing.JTextArea txtTokens;
    private javax.swing.JTextArea txtTraductor;
    // End of variables declaration//GEN-END:variables
}
