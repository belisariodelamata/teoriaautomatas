import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.LinkedList;
import java.util.List;
import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyledDocument;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author BS
 */
public class FrmMain extends javax.swing.JFrame {

    public FrmMain() {
        initComponents();
        iniciarColores();
        btnAnalizarActionPerformed(null);
        timerColor.start();
    }
    int color=0;
    javax.swing.Timer timerColor = new javax.swing.Timer(1000, new ActionListener() {

        public void actionPerformed(ActionEvent e) {
            if (color==0){
                color=1;
            }else{
                color=0;
            }
            for(Token token:ultimosTokenDoWhile){
                if (color==0){
                    colorearTokens(token, attrs);
                }else{
                    colorearTokens(token, attrsBlue);
                }
            }
        }
    });
    SimpleAttributeSet attrs;
    SimpleAttributeSet attrsNormal;
    SimpleAttributeSet attrsRojo;
    SimpleAttributeSet attrsMorado;
    SimpleAttributeSet attrsSalmon;
    SimpleAttributeSet attrsGreen;
    SimpleAttributeSet attrsRed;
    SimpleAttributeSet attrsOp;
    SimpleAttributeSet attrsBlue;

    private void iniciarColores() {
        attrs = new SimpleAttributeSet();
        StyleConstants.setBold(attrs, true);
        StyleConstants.setForeground(attrs, Color.blue);
        attrsNormal = new SimpleAttributeSet();
        StyleConstants.setBold(attrsNormal, false);
        StyleConstants.setForeground(attrsNormal, Color.black);
        attrsRojo = new SimpleAttributeSet();
        StyleConstants.setBold(attrsRojo, false);
        StyleConstants.setForeground(attrsRojo, Color.getHSBColor(200, 220, 800));
        attrsMorado = new SimpleAttributeSet();
        StyleConstants.setBold(attrsMorado, true);
        StyleConstants.setForeground(attrsMorado, Color.MAGENTA);
        //  StyleConstants.setUnderline(attrsMorado, true);
        attrsSalmon = new SimpleAttributeSet();
        StyleConstants.setBold(attrsSalmon, false);
        //StyleConstants.setUnderline(attrsSalmon, true);
        StyleConstants.setForeground(attrsSalmon, Color.ORANGE);
        attrsGreen = new SimpleAttributeSet();
        StyleConstants.setBold(attrsGreen, true);
        StyleConstants.setForeground(attrsGreen, Color.green);
        //StyleConstants.setUnderline(attrsGreen, true);
        attrsRed = new SimpleAttributeSet();
        StyleConstants.setForeground(attrsRed, Color.RED);
        StyleConstants.setItalic(attrsRed, true);
        StyleConstants.setBold(attrsRed, true);
        attrsOp = new SimpleAttributeSet();
        StyleConstants.setForeground(attrsOp, Color.DARK_GRAY);
        StyleConstants.setItalic(attrsOp, true);
        StyleConstants.setBold(attrsOp, true);
        attrsBlue = new SimpleAttributeSet();
        StyleConstants.setForeground(attrsBlue, Color.blue);
        StyleConstants.setItalic(attrsBlue, true);
        StyleConstants.setBold(attrsBlue, true);
    }
    List<Token> ultimosTokenDoWhile=new LinkedList<Token>();
    private void colorearTokens(List<Token> tokens) {
        ultimosTokenDoWhile.clear();
        for (Token token : tokens) {
            //System.out.println(token.toString()+"-->"+token.kind+"-->"+AnalizadorConstants.tokenImage[token.kind]);
            switch (token.kind) {
                case Analizador.Do:
                case Analizador.While:
                    ultimosTokenDoWhile.add(token);
                    colorearTokens(token, attrs);
                    break;
                case Analizador.LKEY:
                case Analizador.RKEY:
                case Analizador.LBRACE:
                case Analizador.RBRACE:
                    colorearTokens(token, attrsRojo);
                    break;
                case Analizador.VARNUM:
                    colorearTokens(token, attrsMorado);
                    break;
                case Analizador.VARCAD:
                    colorearTokens(token, attrsGreen);
                    break;
                case Analizador.STRING_LITERAL:
                    colorearTokens(token, attrsSalmon);
                    break;
                case Analizador.NUM:
                    colorearTokens(token, attrsRed);
                    break;
                case Analizador.MINUS:
                case Analizador.PLUS:
                case Analizador.SLASH:
                case Analizador.STAR:
                    colorearTokens(token, attrsOp);
                    break;
                default:
                    colorearTokens(token, attrsNormal);
            }
        }
        if (!tokens.isEmpty()) {
            Token token = tokens.get(tokens.size() - 1);
            int posicionFinal = posicionesEnLineaCaracter(txtFuncion.getText(), token.endLine, token.endColumn);
            if (posicionFinal >= 0) {
                StyledDocument sd = txtFuncion.getStyledDocument();
                if (sd != null) {
                    sd.setCharacterAttributes(posicionFinal, txtFuncion.getText().length() - posicionFinal + 1,
                            attrsNormal, true);
                }
            }
        }
    }

    private void colorearTokens(Token token, SimpleAttributeSet simpleAttribute) {
        int posicionInicial = posicionesEnLineaCaracter(txtFuncion.getText(), token.beginLine, token.beginColumn);
        int posicionFinal = posicionesEnLineaCaracter(txtFuncion.getText(), token.endLine, token.endColumn);
        StyledDocument sd = txtFuncion.getStyledDocument();
        if (posicionInicial >= 0 && posicionFinal >= 0) {
            if (sd != null) {
                sd.setCharacterAttributes(posicionInicial, posicionFinal - posicionInicial + 1,
                        simpleAttribute, true);
            }
        }

    }

    private String traducir(String msg) {
        msg = msg.replace("Lexical error ", "Error Léxico ");
        msg = msg.replace("at line ", "en línea ");
        msg = msg.replace("column ", "columna ");
        msg = msg.replace("Was expecting one of:", "Se esperaba uno de estos:");
        msg = msg.replace("Was expecting:", "Se esperaba:");
        msg = msg.replace("Encountered \"<EOF>\"", "Se finalizó el texto");
        msg = msg.replace("Encountered", "Se encontró");
        msg = msg.replace("after ", "después");
        return msg;
    }

    private int posicionesEnLineaCaracter(String cadena, int linea, int columna) {
        int posicion = 0;
        String cadenaSplit[] = cadena.split("\\r?\\n");
//        System.out.println("INicio");
        for (int i = 0; i < cadenaSplit.length; i++) {
            ///System.out.println(cadenaSplit[i].length()+"-->"+cadenaSplit[i]);
            if (i != 0) {
                posicion++;
            }
            if (i == linea - 1) {
                posicion += columna;
                break;
            } else {
                posicion += (cadenaSplit[i].length());
            }
        }
        posicion -= 1;
        return posicion;
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        btnAnalizar = new javax.swing.JButton();
        jScrollPane2 = new javax.swing.JScrollPane();
        txtEvaluacion = new javax.swing.JTextArea();
        lblestado = new javax.swing.JLabel();
        jScrollPane3 = new javax.swing.JScrollPane();
        jTextArea1 = new javax.swing.JTextArea();
        lblExpresion1 = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();
        jScrollPane4 = new javax.swing.JScrollPane();
        txtFuncion = new javax.swing.JTextPane();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Analizador Léxico con JavaCC");

        btnAnalizar.setText("Comprobar Sintáxis");
        btnAnalizar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAnalizarActionPerformed(evt);
            }
        });

        txtEvaluacion.setColumns(20);
        txtEvaluacion.setForeground(new java.awt.Color(255, 0, 0));
        txtEvaluacion.setRows(5);
        jScrollPane2.setViewportView(txtEvaluacion);

        lblestado.setBackground(new java.awt.Color(0, 255, 51));
        lblestado.setFont(new java.awt.Font("Tahoma", 3, 11)); // NOI18N
        lblestado.setForeground(new java.awt.Color(255, 255, 255));
        lblestado.setOpaque(true);
        lblestado.setVisible(false);

        jTextArea1.setEditable(false);
        jTextArea1.setColumns(20);
        jTextArea1.setFont(new java.awt.Font("Courier New", 0, 13)); // NOI18N
        jTextArea1.setLineWrap(true);
        jTextArea1.setRows(5);
        jTextArea1.setText("Do{\n\t.....\n}while(condicion);\n---------------------------------\n*Dentro de un ciclo Do while pueden haber más ciclos Do while.\n*Dentro de un ciclo Do while pueden haber variables, asignaciones, comparaciones y operaciones básicas.\n*Para cada fin de operación se finaliza con \";\"\n*Las variables pueden ser de dos tipos:\n  -Numéricas: las cuales comienzan con el símbolo # y debe ir seguida por lo menos por un dígito, letra o guión bajo.\n  -Alfanuméricas: las cuales comienzan con el \nsímbolo $ y debe ir seguida por lo menos por un \ndígito, letra o guión bajo.\n\nLas asignaciones se hacen de la siguiente manera:\nvariable= operaciones basicas |\n\tvariable | numero | \"texto\"\n\nLas operaciones básicas son : \n\t*,/,+,-\nLos símbolos de comparación son: \n\t==,!=,<,<=,>,>=,&&,||\n\n----------------------\nPresentado por:\n-\n-\n-\n-\n-\n-\n");
        jScrollPane3.setViewportView(jTextArea1);

        lblExpresion1.setFont(new java.awt.Font("Tahoma", 3, 11)); // NOI18N
        lblExpresion1.setForeground(new java.awt.Color(255, 0, 0));
        lblExpresion1.setText("Errores");

        jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Teoria de Automatas.jpg"))); // NOI18N

        txtFuncion.setFont(new java.awt.Font("Courier New", 0, 18)); // NOI18N
        txtFuncion.setText("Do{\n $cad=\" Prueba Cadena\";\n}while(2!=3 && (#var==2));");
        txtFuncion.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtFuncionKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtFuncionKeyReleased(evt);
            }
        });
        jScrollPane4.setViewportView(txtFuncion);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 423, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(lblestado, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 489, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(lblExpresion1, javax.swing.GroupLayout.PREFERRED_SIZE, 68, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 499, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(jScrollPane4)))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(527, 527, 527)
                        .addComponent(btnAnalizar, javax.swing.GroupLayout.PREFERRED_SIZE, 177, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jLabel1)))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jLabel1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jScrollPane4, javax.swing.GroupLayout.PREFERRED_SIZE, 301, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(lblestado, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(lblExpresion1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jScrollPane2))
                    .addComponent(jScrollPane3, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 513, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(126, 126, 126)
                .addComponent(btnAnalizar)
                .addContainerGap())
        );

        setBounds(0, 0, 960, 670);
    }// </editor-fold>//GEN-END:initComponents


    private void btnAnalizarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAnalizarActionPerformed
        txtEvaluacion.setText("");
        InputStream is = new ByteArrayInputStream(txtFuncion.getText().replaceAll("\t", " ").getBytes());
        Analizador analizador = null;
        try {
            analizador = new Analizador(is);
            analizador.Input();
            mostrarestado(1, "Expresion evaluada exitosamente");
        } catch (ParseException pe) {
            txtEvaluacion.setText(traducir(pe.getMessage()));
        } catch (TokenMgrError tm) {
            txtEvaluacion.setText(traducir(tm.getMessage()));
        }
        if (analizador != null) {
            colorearTokens(analizador.token_source.getListaToken());
        }

    }//GEN-LAST:event_btnAnalizarActionPerformed

    private void txtFuncionKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtFuncionKeyReleased
        btnAnalizarActionPerformed(null); // TODO add your handling code here:
        timerColor.start();
    }//GEN-LAST:event_txtFuncionKeyReleased

    private void txtFuncionKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtFuncionKeyPressed
        timerColor.stop();
    }//GEN-LAST:event_txtFuncionKeyPressed

    //animacion de estado//////////
    javax.swing.Timer timerestado = new javax.swing.Timer(3000, new ActionListener() {

        public void actionPerformed(ActionEvent e) {
            lblestado.setVisible(false);
        }
    });
    ///////////////////////////////

    private void mostrarestado(int estado, String msg) {
        timerestado.stop();
        if (estado == 1) {
            lblestado.setBackground(Color.blue);
            lblestado.setIcon(new javax.swing.ImageIcon(getClass().getResource("1.gif")));
        } else {
            lblestado.setBackground(Color.red);
            lblestado.setIcon(new javax.swing.ImageIcon(getClass().getResource("2.gif")));
        }
        lblestado.setText(msg);
        lblestado.setVisible(true);
        timerestado.start();
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /*
         * Set the Nimbus look and feel
         */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /*
         * If Nimbus (introduced in Java SE 6) is not available, stay with the
         * default look and feel. For details see
         * http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(FrmMain.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(FrmMain.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(FrmMain.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(FrmMain.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /*
         * Create and display the form
         */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new FrmMain().setVisible(true);
            }
        });
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAnalizar;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JScrollPane jScrollPane4;
    private javax.swing.JTextArea jTextArea1;
    private javax.swing.JLabel lblExpresion1;
    private javax.swing.JLabel lblestado;
    private javax.swing.JTextArea txtEvaluacion;
    private javax.swing.JTextPane txtFuncion;
    // End of variables declaration//GEN-END:variables
}
